function [X, solutionStatus] = solveSDP(A, b, c, K, solver)
% function that solves an SDP with Input in SeDuMi format with a specified
% solver
% Call:
%   [X, solutionStatus] = solveSDP(A, b, c, K, solver)
% Input:
%   A, b, c, K      SeDuMi Input for an SDP
%   solver          1 ... SeDuMi
%                   2 ... MOSEK
% Output:
%   X               optimal solution of SDP (already as matrix)
%   solutionStatus  -2 ... run into numerical problems, but still there is a
%                               solution
%                   -1 ... problem is infeasible
%                    0 ... optimal solution found
%
% Elisabeth Gaar 2019, elisabeth.gaar@aau.at

solutionStatus = 0;

if( solver == 2)
    clear prob; 
    [~, returnCode, res] = evalc('mosekopt(''symbcon'')');
    prob.bardim    = [K.s];

    %Define objective function
    Cbar = reshape(c,sqrt(length(c)),[]);
    CbarL = tril(Cbar);
    [subkTemp,sublTemp,valTemp] = find(CbarL);
    prob.barc.subj = ones(size(subkTemp')); 
    prob.barc.subk = subkTemp';
    prob.barc.subl = sublTemp';
    prob.barc.val  = valTemp';

    %Define constraints
    subk = [];
    subl = [];
    val = [];
    subi = [];
    for i = 1:size(A,1)
        Abari = reshape(A(i,:),sqrt(length(A(i,:))),[]);
        [subkTemp,sublTemp,valTemp] = find(tril(Abari));
        subk = [subk,subkTemp'];
        subl = [subl,sublTemp'];
        val = [val,valTemp'];
        subi = [subi, i*ones(size(subkTemp'))];
    end
    prob.bara.subj = ones(size(subk));
    prob.bara.subi = subi;
    prob.bara.subk = subk;
    prob.bara.subl = subl;
    prob.bara.val  = val;

    prob.c   = zeros(1,0); 
    prob.a   = zeros(size(A,1),0);
    prob.blc = b; 
    prob.buc = b;
    prob.blx = zeros(0,1);
    prob.bux = zeros(0,1);

    [returnCode, res] = mosekopt('minimize info',prob);
    if(returnCode ~= 0)
        str = sprintf(['Mosek encountered problems! ',... 
            'Problem: %s, Solution status: %s, %s \n'],res.rcodestr, ...
            res.sol.itr.solsta, res.sol.itr.prosta);
        disp(str)
    end
    if(strcmp(res.sol.itr.prosta,'PRIMAL_INFEASIBLE') == 1)
        X = 0;
        solutionStatus = -1;
        return;
    elseif(strcmp(res.sol.itr.solsta,'NEAR_OPTIMAL') == 1)
        solutionStatus = -2;
    end

    X = zeros(K.s,K.s);
    mask = tril(ones(K.s,K.s));
    X(mask==1) = res.sol.itr.barx;
    X = X + X' - diag(diag(X));
    
else
    [x,y,info] = sedumi(A,b,c,K);
    
    if info.pinf
        X = 0;
        solutionStatus = -1;
        return;
    end
    X=mat(x);
end


end

