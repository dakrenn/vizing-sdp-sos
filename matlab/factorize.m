function S=factorize(X)
% factorize X such that X =S'*S after setting all eigenvalues <=
% eps to zero.
% S = factoriz(X);
% called from compute_certificate.m
%
% 2019, Elisabeth Gaar, elisabeth.gaar@aau.at and Angelika Wiegele, angelika.wiegele@aau.at

eps = 0.01;
[V,D] = eig(X);

diagD = diag(D);
indices = (diagD>eps);
S = diag(sqrt(diagD(indices)))*(V(:,indices))';

normErr = norm(S'*S - X);
fprintf('||S^tS - X||: %f\n',normErr);

