function [finalS,S,X,solutionStatus,SHist,XHist] = ...
    compute_certificate(nG,kG,nH,kH,ell,solver,...
    removeUnusedMonomials, ...
    usePrecalculatedReducedIndices, precalculatedIndices,...
    saveFigure);
%
% matlab routine to set up an SDP and compute a numeric certificate
% for given nG, kG, nH, kH as described in the paper 
%
% @article{Gaar-Krenn-Margulies-Wiegele,
%  author = {Elisabeth Gaar and Daniel Krenn and Susan Margulies and Angelika Wiegele},
%  title = {An Optimization-Based Sum-of-Squares Approach to {V}izing's Conjecture},
%  arXiv = 
% }
%
% Call:
%   [finalS,S,X,solutionStatus,SHist,XHist] = compute_certificate(nG,kG,nH,kH,ell,solver,...
%    removeUnusedMonomials, ...
%    usePrecalculatedReducedIndices, precalculatedIndices,...
%    saveFigure);
%
% Input:
%   nG, kG, nH, kH  ... parameters of the graph classes
%   ell             ... desired degree of certificate
%   solver          1 ... SeDuMi
%                   2 ... MOSEK
%   removeUnusedMonomials ... flag (0/1) indicating whether
%                             "apparently" unused monomials should
%                             be removed 
%   usePrecalculatedReducedIndices ... flag (0/1)
%   precalculatedIndices ... precalculated indices, used in remove_zero_cols.m
%   saveFigure      ...  flag (0/1) if figure should be saved
%   
% Output:
%   finalS          ... final factorized solution, indexed by all
%                       given monomials
%   S               ... factorization of X (maybe with some
%                       monomials removed)
%   X               ... optimal solution of SDP
%   solutionStatus  from solving the SDP
%                   -2 ... run into numerical problems, but still there is a
%                               solution
%                   -1 ... problem is infeasible
%                    0 ... optimal solution found
%   SHist, XHist    ... all the S and X matrices calculated 
%
% 2019, Elisabeth Gaar, elisabeth.gaar@aau.at and Angelika Wiegele, angelika.wiegele@aau.at



red = 1; %1 ... True, 0 ... False
orderStr = 'deglex';
inverted = 0;  % 1 ... True, 0 ... False
fix = 1; % 1 ... True, 0 ... False

XHist = cell(1);
SHist = cell(1);
finalS = [];
S = [];
X = [];


if(red == 1)
  redStr = 'True';
else
  redStr = 'False';
end
if inverted
  invStr = 'True';
else
  invStr = 'False';
end
if(fix == 1)
    fixStr = 'True';
else
    fixStr = 'False';
end

% read in constraints from file provided by SageMath computations 
filename = ['sdp_nG',num2str(nG),'_kG',num2str(kG),...
    '_nH',num2str(nH),'_kH',num2str(kH),'_ell',num2str(ell),...
    '_',orderStr,'_red',redStr,'_inv',invStr,'_fix',fixStr,'_'];
filenameA = [filename,'A'];
filenameb = [filename,'b'];

load(filenameA);
load(filenameb);

A = spconvert(eval(filenameA));
b = spconvert(eval(filenameb));

n = sqrt(size(A,2));
K.s = n;

% choose an objective function, there is no clear strategy which is
% best -- play around with different versions
if(kG == nG && kH == nH - 1)
    C = zeros(n);
elseif(kG == nG && kH == nH - 2)
    C = -eye(n);
elseif(kG == nG - 1 && nH == 3 && kH == 2)
    C = ones(n);
    C(1:nG*nH+1,1:nG*nH+1) = 2;
    C = -C;
else
    C = ones(n);
    C(1:nG*nH+1,1:nG*nH+1) = 2;
    C = -C;
end

c = C(:);

fprintf('Size of SDP: n = %d, m = %d\n',n,size(A,1));
nrConSDP = size(A,1);

monomials = 1:n;
eps = 1e-3;
count = 0;
repeat = 1;
while (count == 0 || repeat >= 1)

    % iteratively solve SDP and remove monomials if they seem to be
    % not needed.
    if((removeUnusedMonomials == 1 && count > 0) || ...
            (usePrecalculatedReducedIndices == 1 && count == 0))
        [A, b, C, K, monomials] = remove_zero_cols(A,b,C,K,S,eps,monomials,...
            usePrecalculatedReducedIndices, precalculatedIndices);
        c = C(:);
        disp('------------------------------------------------------')
    end

    solveSDPTic = tic;
    [X, solutionStatus] = solveSDP(A,b,c,K,solver);
    disp('------------------------------------------------------')
    fprintf('Time needed for solving the SDP: %f\n', toc(solveSDPTic));
    if(solutionStatus == -1)
        disp('SDP infeasible, no certificate found.');
        return;
    elseif(solutionStatus == -2)
        disp('Run into numerical problems!');
    end


    S = factorize(X);
    figure;
    image(S,'CDataMapping','scaled'); colorbar
    if(saveFigure == 1)
        name = [num2str(nG),'_',num2str(kG),'_',...
            num2str(nH),'_',num2str(kH),...
            '_',num2str(ell),'_',num2str(count)]; 
        if(usePrecalculatedReducedIndices == 1)
            name = [name,'_FixedMonomials'];
        end
        saveas(gcf,['Figures/',name,'.png'])
        saveas(gcf,['Figures/',name,'.fig'])
    end
    
    XHist{count+1} = X;
    SHist{count+1} = S;
    
    count = count + 1;
    repeat = sum(all(abs(S) < eps));
    if(removeUnusedMonomials == 0)
        repeat = 0;
    end

end


finalS = zeros(size(S,1),n);
finalS(:,monomials) = S;

end
