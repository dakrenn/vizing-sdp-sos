function [A_reduced, b_reduced, C_reduced, K_reduced, monomials_reduced] = ...
    remove_zero_cols(A,b,C,K,S,eps,monomials,...
    usePrecalculatedReducedIndices, precalculatedIndices);
% function that removes from matrix S columns that are considered
% zero and sets up a new SDP that ignores these columns (i.e.,
% these monomials) and removes the zero-rows from the constraints. 
% On return you obtain new data (A,b,C,K)_reduced for solving the
% SDP and monomials_reduced, which tells you which monomials are
% left. (Start counting with 1.)
% call:
% [A_reduced, b_reduced, C_reduced, K_reduced, monomials_reduced] = remove_zero_cols(A,b,C,K,S,eps,monomials);
% or
% [A_reduced, b_reduced, C_reduced, K_reduced, monomials_reduced] = remove_zero_cols(A,b,C,K,S);
% with default values: monomials = 1:n; eps = 1e-3;
%
% called from compute_certificate.m
%
% 2019, Elisabeth Gaar, elisabeth.gaar@aau.at and Angelika Wiegele, angelika.wiegele@aau.at

assert(nargin >= 5,'Too few input arguments.');

m = size(A,1);
n = size(C,1);


if nargin <= 5
    eps = 1e-3;
end
if nargin <= 6
  monomials = 1:n;
end
if nargin <= 7
    usePrecalculatedReducedIndices = 0;
end
if nargin <= 8
    precalculatedIndices = [];
end


if(usePrecalculatedReducedIndices == 1)
    ind = precalculatedIndices;
else
    I = 1 - all(abs(S) < eps);
    ind = find(I);
end
monomials_reduced = monomials(ind);
nrNewVar = length(ind);
fprintf('Number of remaining monomials in the SDP: %d (from %d)\n',nrNewVar, n);

At_reduced = [];
for i = 1:m
  B = reshape(A(i,:),n,n);
  B = B(ind,ind);
  At_reduced = [At_reduced B(:)];
end
C_reduced = C(ind,ind);

J = all(At_reduced == 0);
% check to only remove constraints with rhs 0:
ind = find(J);
assert(sum(b(ind))==0,'Removed too many variables/constraints.');

J = 1-J;
ind = find(J);

At_reduced = At_reduced(:,ind);

A_reduced = sparse(At_reduced');
b_reduced = b(ind);
K_reduced.s = nrNewVar;

