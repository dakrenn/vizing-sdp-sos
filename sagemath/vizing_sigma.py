r"""

::

    sage: from vizing_sigma import VizingSigma
    sage: P.<alpha, beta, gamma, delta, epsilon, n> = PolynomialRing(QQ, order='deglex')

::

    sage: Sigma = VizingSigma(n, 1)
    sage: one = Sigma.one()
    sage: s_g = Sigma.FormalSum([(1, Sigma(('g', 1))),
    ....:                        (-(n-1), one)])
    sage: s_g^2
    (-n+1)*1 + sigma(g, 1)

    sage: s_g = Sigma.FormalSum([(alpha, one),
    ....:                        (beta, Sigma(('g', 1)))])
    sage: s_g^2
    (-beta^2*n^2+beta^2*n+alpha^2)*1
    + (2*beta^2*n+2*alpha*beta-beta^2)*sigma(g, 1)
    sage: [c for c, x in s_g^2 - Sigma.FormalSum([(-(n-1), one),
    ....:                                         (1, Sigma(('g', 1)))])]
    [-beta^2*n^2 + beta^2*n + alpha^2 + n - 1,
     2*beta^2*n + 2*alpha*beta - beta^2 - 1]
    sage: I = P.ideal(_ + [gamma, delta, epsilon])
    sage: I.dimension()
    1

    sage: [p for p in I.groebner_basis()]
    [alpha^4 - 2*alpha^2*n^2 + n^4 + 4*alpha^2*n - 4*n^3 - 2*alpha^2 + 6*n^2 - 4*n + 1,
     alpha^3*beta + 2*alpha^2*n - n^3 - 5/2*alpha^2 - 1/2*alpha*beta - 1/4*beta^2 + 7/2*n^2 - 9/2*n + 9/4,
     beta*n^3 + 1/2*alpha^2*beta + alpha*n^2 - 5/2*beta*n^2 - alpha*n + 2*beta*n - 1/2*beta,
     alpha*beta^2 - 2*beta*n^2 - 2*alpha*n + 4*beta*n + alpha - 2*beta,
     alpha*beta*n + alpha^2 - 1/2*alpha*beta + 1/4*beta^2 + 1/2*n - 3/4,
     beta^3 + 2*beta*n + 2*alpha - 3*beta,
     beta^2*n + alpha*beta - 1/2*beta^2 - 1/2,
     gamma,
     delta,
     epsilon]

::

    sage: Sigma = VizingSigma(n, 2)
    sage: one = Sigma.one()
    sage: s_g = Sigma.FormalSum([(alpha, one),
    ....:                        (beta, Sigma(('g', 1))),
    ....:                        (gamma, Sigma(('g', 2)))])
    sage: s_g^2
    (3/4*gamma^2*n^4+beta*gamma*n^3-7/2*gamma^2*n^3-3*beta*gamma*n^2+21/4*gamma^2*n^2+2*beta*gamma*n-5/2*gamma^2*n+alpha^2)*1 + (-2*gamma^2*n^3-3*beta*gamma*n^2+9*gamma^2*n^2+9*beta*gamma*n-13*gamma^2*n+2*alpha*beta+beta^2-6*beta*gamma+6*gamma^2)*sigma(g, 1) + (3*gamma^2*n^2+6*beta*gamma*n-9*gamma^2*n+2*alpha*gamma+2*beta^2-8*beta*gamma+7*gamma^2)*sigma(g, 2)
    sage: [c for c, x in s_g^2 - Sigma.FormalSum([(-(n-2), one),
    ....:                                         (1, Sigma(('g', 1)))])]
    [3/4*gamma^2*n^4 + beta*gamma*n^3 - 7/2*gamma^2*n^3 - 3*beta*gamma*n^2 + 21/4*gamma^2*n^2 + 2*beta*gamma*n - 5/2*gamma^2*n + alpha^2 + n - 2,
     -2*gamma^2*n^3 - 3*beta*gamma*n^2 + 9*gamma^2*n^2 + 9*beta*gamma*n - 13*gamma^2*n + 2*alpha*beta + beta^2 - 6*beta*gamma + 6*gamma^2 - 1,
     3*gamma^2*n^2 + 6*beta*gamma*n - 9*gamma^2*n + 2*alpha*gamma + 2*beta^2 - 8*beta*gamma + 7*gamma^2]
    sage: I = P.ideal(_ + [delta, epsilon])
    sage: I.dimension()
    1

    sage: I.groebner_basis()
    Polynomial Sequence with 26 Polynomials in 6 Variables

::

    sage: Sigma = VizingSigma(n, 3)
    sage: one = Sigma.one()
    sage: s_g = Sigma.FormalSum([(alpha, one),
    ....:                        (beta, Sigma(('g', 1))),
    ....:                        (gamma, Sigma(('g', 2))),
    ....:                        (delta, Sigma(('g', 3)))])
    sage: [c for c, x in s_g^2 - Sigma.FormalSum([(-(n-3), one),
    ....:                                         (1, Sigma(('g', 1)))])]
    [-5/18*delta^2*n^6 - 2/3*gamma*delta*n^5 + 19/6*delta^2*n^5 - 1/3*beta*delta*n^4 - 1/4*gamma^2*n^4 + 17/3*gamma*delta*n^4 - 127/9*delta^2*n^4 + 2*beta*delta*n^3 + 3/2*gamma^2*n^3 - 52/3*gamma*delta*n^3 + 61/2*delta^2*n^3 - 11/3*beta*delta*n^2 - 11/4*gamma^2*n^2 + 67/3*gamma*delta*n^2 - 569/18*delta^2*n^2 + 2*beta*delta*n + 3/2*gamma^2*n - 10*gamma*delta*n + 37/3*delta^2*n + alpha^2 + n - 3,
     delta^2*n^5 + 5/2*gamma*delta*n^4 - 45/4*delta^2*n^4 + 4/3*beta*delta*n^3 + gamma^2*n^3 - 21*gamma*delta*n^3 + 99/2*delta^2*n^3 - 8*beta*delta*n^2 - 6*gamma^2*n^2 + 127/2*gamma*delta*n^2 - 423/4*delta^2*n^2 + 44/3*beta*delta*n + 11*gamma^2*n - 81*gamma*delta*n + 217/2*delta^2*n + 2*alpha*beta + beta^2 - 8*beta*delta - 6*gamma^2 + 36*gamma*delta - 42*delta^2 - 1,
     -5/2*delta^2*n^4 - 20/3*gamma*delta*n^3 + 25*delta^2*n^3 - 4*beta*delta*n^2 - 3*gamma^2*n^2 + 48*gamma*delta*n^2 - 187/2*delta^2*n^2 + 20*beta*delta*n + 15*gamma^2*n - 340/3*gamma*delta*n + 155*delta^2*n + 2*alpha*gamma + 2*beta^2 + 4*beta*gamma - 24*beta*delta - 17*gamma^2 + 88*gamma*delta - 96*delta^2,
     10/3*delta^2*n^3 + 10*gamma*delta*n^2 - 25*delta^2*n^2 + 8*beta*delta*n + 6*gamma^2*n - 46*gamma*delta*n + 191/3*delta^2*n + 2*alpha*delta + 6*beta*gamma - 18*beta*delta - 12*gamma^2 + 54*gamma*delta - 55*delta^2]
    sage: I = P.ideal(_ + [epsilon])
    sage: I.dimension()
    1

::

    sage: Sigma = VizingSigma(n, 4)
    sage: one = Sigma.one()
    sage: s_g = Sigma.FormalSum([(alpha, one),
    ....:                        (beta, Sigma(('g', 1))),
    ....:                        (gamma, Sigma(('g', 2))),
    ....:                        (delta, Sigma(('g', 3))),
    ....:                        (epsilon, Sigma(('g', 4)))])
    sage: [c for c, x in s_g^2 - Sigma.FormalSum([(-(n-4), one),
    ....:                                         (1, Sigma(('g', 1)))])]
    [35/576*epsilon^2*n^8 + 5/24*delta*epsilon*n^7 - 185/144*epsilon^2*n^7 + 5/24*gamma*epsilon*n^6 + 5/36*delta^2*n^6 - 85/24*delta*epsilon*n^6 + 3295/288*epsilon^2*n^6 + 1/12*beta*epsilon*n^5 + 1/6*gamma*delta*n^5 - 67/24*gamma*epsilon*n^5 - 11/6*delta^2*n^5 + 587/24*delta*epsilon*n^5 - 997/18*epsilon^2*n^5 - 5/6*beta*epsilon*n^4 - 5/3*gamma*delta*n^4 + 115/8*gamma*epsilon*n^4 + 335/36*delta^2*n^4 - 2095/24*delta*epsilon*n^4 + 90155/576*epsilon^2*n^4 + 35/12*beta*epsilon*n^3 + 35/6*gamma*delta*n^3 - 845/24*gamma*epsilon*n^3 - 45/2*delta^2*n^3 + 505/3*delta*epsilon*n^3 - 36875/144*epsilon^2*n^3 - 25/6*beta*epsilon*n^2 - 25/3*gamma*delta*n^2 + 485/12*gamma*epsilon*n^2 + 230/9*delta^2*n^2 - 985/6*delta*epsilon*n^2 + 10655/48*epsilon^2*n^2 + 2*beta*epsilon*n + 4*gamma*delta*n - 17*gamma*epsilon*n - 32/3*delta^2*n + 62*delta*epsilon*n - 309/4*epsilon^2*n + alpha^2 + n - 4,
     -5/18*epsilon^2*n^7 - 35/36*delta*epsilon*n^6 + 35/6*epsilon^2*n^6 - gamma*epsilon*n^5 - 2/3*delta^2*n^5 + 197/12*delta*epsilon*n^5 - 929/18*epsilon^2*n^5 - 5/12*beta*epsilon*n^4 - 5/6*gamma*delta*n^4 + 40/3*gamma*epsilon*n^4 + 35/4*delta^2*n^4 - 4055/36*delta*epsilon*n^4 + 745/3*epsilon^2*n^4 + 25/6*beta*epsilon*n^3 + 25/3*gamma*delta*n^3 - 205/3*gamma*epsilon*n^3 - 265/6*delta^2*n^3 + 4795/12*delta*epsilon*n^3 - 6280/9*epsilon^2*n^3 - 175/12*beta*epsilon*n^2 - 175/6*gamma*delta*n^2 + 500/3*gamma*epsilon*n^2 + 425/4*delta^2*n^2 - 13795/18*delta*epsilon*n^2 + 6815/6*epsilon^2*n^2 + 125/6*beta*epsilon*n + 125/3*gamma*delta*n - 572/3*gamma*epsilon*n - 721/6*delta^2*n + 744*delta*epsilon*n - 2941/3*epsilon^2*n + 2*alpha*beta + beta^2 - 10*beta*epsilon - 20*gamma*delta + 80*gamma*epsilon + 50*delta^2 - 280*delta*epsilon + 340*epsilon^2 - 1,
     35/36*epsilon^2*n^6 + 7/2*delta*epsilon*n^5 - 77/4*epsilon^2*n^5 + 15/4*gamma*epsilon*n^4 + 5/2*delta^2*n^4 - 55*delta*epsilon*n^4 + 2845/18*epsilon^2*n^4 + 5/3*beta*epsilon*n^3 + 10/3*gamma*delta*n^3 - 275/6*gamma*epsilon*n^3 - 30*delta^2*n^3 + 685/2*delta*epsilon*n^3 - 8255/12*epsilon^2*n^3 - 15*beta*epsilon*n^2 - 30*gamma*delta*n^2 + 825/4*gamma*epsilon*n^2 + 265/2*delta^2*n^2 - 1055*delta*epsilon*n^2 + 60155/36*epsilon^2*n^2 + 130/3*beta*epsilon*n + 260/3*gamma*delta*n - 2425/6*gamma*epsilon*n - 255*delta^2*n + 1604*delta*epsilon*n - 12857/6*epsilon^2*n + 2*alpha*gamma + 2*beta^2 + 4*beta*gamma - 40*beta*epsilon + gamma^2 - 80*gamma*delta + 290*gamma*epsilon + 180*delta^2 - 960*delta*epsilon + 1130*epsilon^2,
     -7/3*epsilon^2*n^5 - 35/4*delta*epsilon*n^4 + 245/6*epsilon^2*n^4 - 10*gamma*epsilon*n^3 - 20/3*delta^2*n^3 + 235/2*delta*epsilon*n^3 - 860/3*epsilon^2*n^3 - 5*beta*epsilon*n^2 - 10*gamma*delta*n^2 + 100*gamma*epsilon*n^2 + 65*delta^2*n^2 - 2365/4*delta*epsilon*n^2 + 6055/6*epsilon^2*n^2 + 35*beta*epsilon*n + 70*gamma*delta*n - 330*gamma*epsilon*n - 625/3*delta^2*n + 2645/2*delta*epsilon*n - 1781*epsilon^2*n + 2*alpha*delta + 6*beta*gamma + 6*beta*delta - 60*beta*epsilon + 6*gamma^2 - 114*gamma*delta + 360*gamma*epsilon + 221*delta^2 - 1110*delta*epsilon + 1260*epsilon^2,
     35/12*epsilon^2*n^4 + 35/3*delta*epsilon*n^3 - 245/6*epsilon^2*n^3 + 15*gamma*epsilon*n^2 + 10*delta^2*n^2 - 115*delta*epsilon*n^2 + 2605/12*epsilon^2*n^2 + 10*beta*epsilon*n + 20*gamma*delta*n - 95*gamma*epsilon*n - 60*delta^2*n + 1150/3*delta*epsilon*n - 3115/6*epsilon^2*n + 2*alpha*epsilon + 8*beta*delta - 32*beta*epsilon + 6*gamma^2 - 56*gamma*delta + 152*gamma*epsilon + 92*delta^2 - 432*delta*epsilon + 471*epsilon^2]
    sage: I = P.ideal(_ + [])
    sage: I.dimension()  # not tested
    1
"""
from __future__ import print_function
from __future__ import absolute_import

from sage.structure.element import CommutativeAlgebraElement
from sage.structure.element import MultiplicativeGroupElement
from sage.structure.parent import Parent
from sage.structure.unique_representation import UniqueRepresentation
from sage.structure.formal_sum import FormalSum, FormalSums

from sage.structure.richcmp import richcmp_by_eq_and_lt


class FormalSumPP(FormalSum, CommutativeAlgebraElement):
    def __init__(self, data, **kwds):
        def flatten(c, x):
            if isinstance(x, (list, FormalSum)):
                return [(c*d, y) for d, y in x]
            else:
                return [(c, x)]
        data = sum((flatten(c, x) for c, x in data), [])
        super(FormalSumPP, self).__init__(data, **kwds)

    def _mul_(self, other):
        return self.__class__([(c*d, x*y) for c, x in self for d, y in other],
                              parent=self.parent())

    def reduced(self, **kwds):
        return self.__class__([(c, x.reduced(**kwds)) for c, x in self],
                              parent=self.parent())


class SigmaSymmetric(MultiplicativeGroupElement):
    def __init__(self, parent, marker, degree):
        if parent is None:
            raise ValueError('parent must be provided')
        super(SigmaSymmetric, self).__init__(parent=parent)

        self.degree = degree
        if self.is_one():
            marker = None
        self.marker = marker

    def is_one(self):
        return self.degree == 0

    def reduced(self, recursive=True):
        r"""
        ::

            sage: from vizing_sigma import VizingSigma
            sage: P.<n> = QQ[]

        ::

            sage: Sigma = VizingSigma(n, 1)

            sage: Sigma(('g', 0))
            1
            sage: Sigma(('g', 1)) 
            sigma(g, 1)
            sage: Sigma(('g', 2))
            (-1/2*n^2+1/2*n)*1 + (n-1)*sigma(g, 1)

            sage: Sigma(('g', 3))
            (-1/3*n^3+n^2-2/3*n)*1 + (1/2*n^2-3/2*n+1)*sigma(g, 1)
            sage: Sigma(('g', 3), reduce=False)
            sigma(g, 3)
            sage: _.reduced(recursive=False)
            (-1/6*n^2+1/2*n-1/3)*sigma(g, 1) + (2/3*n-4/3)*sigma(g, 2)
            sage: _.reduced(recursive=False)
            (-1/3*n^3+n^2-2/3*n)*1 + (1/2*n^2-3/2*n+1)*sigma(g, 1)
            sage: _ == Sigma(('g', 3))
            True

            sage: Sigma(('g', 4 ))
            (-1/8*n^4+3/4*n^3-11/8*n^2+3/4*n)*1
            + (1/6*n^3-n^2+11/6*n-1)*sigma(g, 1)

        ::

            sage: Sigma = VizingSigma(n, 2)

            sage: Sigma(('g', 0))
            1
            sage: Sigma(('g', 1))
            sigma(g, 1)
            sage: Sigma(('g', 2))
            sigma(g, 2)

            sage: Sigma(('g', 3))
            (1/6*n^3-1/2*n^2+1/3*n)*1
            + (-1/2*n^2+3/2*n-1)*sigma(g, 1)
            + (n-2)*sigma(g, 2)
            sage: Sigma(('g', 3), reduce=False)
            sigma(g, 3)
            sage: _.reduced(recursive=False)
            (1/6*n^3-1/2*n^2+1/3*n)*1
            + (-1/2*n^2+3/2*n-1)*sigma(g, 1)
            + (n-2)*sigma(g, 2)
            sage: _ == Sigma(('g', 3))
            True

            sage: Sigma(('g', 4))
            (1/8*n^4-3/4*n^3+11/8*n^2-3/4*n)*1
            + (-1/3*n^3+2*n^2-11/3*n+2)*sigma(g, 1)
            + (1/2*n^2-5/2*n+3)*sigma(g, 2)
            sage: Sigma(('g', 4), reduce=False)
            sigma(g, 4)
            sage: _.reduced(recursive=False)
            (1/24*n^3-1/4*n^2+11/24*n-1/4)*sigma(g, 1)
            + (-1/4*n^2+5/4*n-3/2)*sigma(g, 2)
            + (3/4*n-9/4)*sigma(g, 3)
            sage: _.reduced(recursive=False)
            (1/8*n^4-3/4*n^3+11/8*n^2-3/4*n)*1
            + (-1/3*n^3+2*n^2-11/3*n+2)*sigma(g, 1)
            + (1/2*n^2-5/2*n+3)*sigma(g, 2)
            sage: _ == Sigma(('g', 4))
            True
        """
        parent = self.parent()
        d = parent.d
        degree = self.degree

        if degree < d + 1:
            return self
        
        from sage.arith.srange import srange
        from sage.functions.other import binomial

        n = parent.n
        marker = self.marker

        j = degree - d - 1
        c = binomial(n, d+j+1)
        def reduce_it(data):
            if recursive:
                return data.reduced()
            else:
                return data
        return parent.FormalSum(
            [(c * (-1)**(d+r) * binomial(d+1, r) / binomial(n, j+r),
              reduce_it(parent.element_class(parent, marker, j+r)))
             for r in srange(d+1)])

    def _repr_(self):
        if self.is_one():
            return '1'
        return 'sigma({}, {})'.format(self.marker, self.degree)

    def _key_(self):
        return (self.marker, self.degree)

    def __hash__(self):
        return hash(self._key_())

    _richcmp_ = richcmp_by_eq_and_lt("_eq_", "_lt_")

    def _eq_(self, other):
        return self._key_() == other._key_()

    def _lt_(self, other):
        return self._key_() < other._key_()

    def _mul_(self, other, reduce=True):
        r"""
        ::

            sage: from vizing_sigma import VizingSigma
            sage: P.<n> = QQ[]

        ::

            sage: Sigma = VizingSigma(n, 1)

            sage: Sigma(('g', 1))^2
            (-n^2+n)*1 + (2*n-1)*sigma(g, 1)
            sage: Sigma(('g', 1))._mul_(Sigma(('g', 1)), reduce=False)
            sigma(g, 1) + 2*sigma(g, 2)
            sage: _.reduced(recursive=False)
            (-n^2+n)*1 + (2*n-1)*sigma(g, 1)
            sage: _ == Sigma(('g', 1))^2
            True

        ::

            sage: Sigma = VizingSigma(n, 2)

            sage: Sigma(('g', 1))^2
            sigma(g, 1) + 2*sigma(g, 2)

            sage: Sigma(('g', 2)) * Sigma(('g', 1))
            (1/2*n^3-3/2*n^2+n)*1
            + (-3/2*n^2+9/2*n-3)*sigma(g, 1)
            + (3*n-4)*sigma(g, 2)
            sage: Sigma(('g', 2))._mul_(Sigma(('g', 1)), reduce=False)
            2*sigma(g, 2) + 3*sigma(g, 3)
            sage: _.reduced(recursive=False)
            (1/2*n^3-3/2*n^2+n)*1
            + (-3/2*n^2+9/2*n-3)*sigma(g, 1)
            + (3*n-4)*sigma(g, 2)
            sage: _ == Sigma(('g', 2)) * Sigma(('g', 1))
            True

            sage: Sigma(('g', 2))^2
            (3/4*n^4-7/2*n^3+21/4*n^2-5/2*n)*1
            + (-2*n^3+9*n^2-13*n+6)*sigma(g, 1)
            + (3*n^2-9*n+7)*sigma(g, 2)
            sage: Sigma(('g', 2))._mul_(Sigma(('g', 2)), reduce=False)
            sigma(g, 2) + 6*sigma(g, 3) + 6*sigma(g, 4)
            sage: _.reduced(recursive=False)
            (n^3-3*n^2+2*n)*1
            + (1/4*n^3-9/2*n^2+47/4*n-15/2)*sigma(g, 1)
            + (-3/2*n^2+27/2*n-20)*sigma(g, 2)
            + (9/2*n-27/2)*sigma(g, 3)
            sage: _.reduced(recursive=False)
            (3/4*n^4-7/2*n^3+21/4*n^2-5/2*n)*1
            + (-2*n^3+9*n^2-13*n+6)*sigma(g, 1)
            + (3*n^2-9*n+7)*sigma(g, 2)
            sage: _ == Sigma(('g', 2))^2
            True
        """
        if other.is_one():
            return self
        if self.is_one():
            return other
        if self.marker != other.marker:
            raise NotImplementedError(
                'cannot multiply {} and {} as they '
                'have different markers {} and {}'.format(
                    self, other, self.marker, other.marker))

        from sage.arith.srange import srange
        from sage.functions.other import binomial

        def reduce_it(data):
            if reduce:
                return data.reduced()
            else:
                return data

        parent = self.parent()
        n = parent.n
        i = self.degree
        j = other.degree
        return parent.FormalSum([(binomial(j, r) * binomial(i+r, j),
                                  reduce_it(parent.element_class(parent,
                                                                 self.marker,
                                                                 i+r)))
                                 for r in srange(min(j, n-i)+1)])


class VizingSigma(UniqueRepresentation, Parent):

    Element = SigmaSymmetric

    def __init__(self, n, d):
        r"""
        ::

            sage: from vizing_sigma import VizingSigma
            sage: P.<n> = QQ[]
            sage: VizingSigma(n, 1)
            VizingSigma with n=n, k=n - 1 over
            Univariate Polynomial Ring in n over Rational Field
        """
        from sage.categories.monoids import Monoids
        super(VizingSigma, self).__init__(base=n.parent(),
                                          category=Monoids())
        self.n = n
        self.d = d

    def _repr_(self):
        return 'VizingSigma with n={}, k={} over {}'.format(
            self.n, self.n - self.d, self.base())

    def _element_constructor_(self, data, reduce=True):
        r"""
        ::

            sage: from vizing_sigma import VizingSigma
            sage: P.<n> = QQ[]
            sage: Sigma = VizingSigma(n, 1)
            sage: Sigma(('g', 1)) 
            sigma(g, 1)
        """
        if isinstance(data, self.element_class):
            if self.n == data.parent().n:
                return data

        elif isinstance(data, tuple):
            marker, degree = data
            element = self.element_class(self, marker, degree)
            if reduce:
                return element.reduced()
            else:
                return element

        raise ValueError('{} not in {}').format(data, self)

    def FormalSum(self, data, **kwds):
        formal_sums = FormalSums(self.base())
        return FormalSumPP(data, parent=formal_sums, **kwds)

    def one(self):
        return self.element_class(self, None, 0)
