
class Logger(object):
    def __init__(self, name):
        import logging
        self._name = name
        self._level = logging.WARNING
        self._filename = None
    def level(self, level=None):
        if level is None:
            return self.level
        self._level = level
    def filename(self, filename):
        if filename != self._filename:
            if filename is not None:
                with open(filename, 'w') as f:
                    pass
            self._filename = filename
    def log(self, level, levelname, format, *args):
        from datetime import datetime
        if level >= self._level:
            msg = (('%s %s:%s ' % (datetime.now(), levelname, self._name))
                   + (format % args))
            print(msg)
            if self._filename is not None:
                with open(self._filename, 'a') as f:
                    f.write(msg + '\n')
    def warning(self, format, *args):
        import logging
        self.log(logging.WARNING, 'WARNING', format, *args)
    def info(self, format, *args):
        import logging
        self.log(logging.INFO, 'INFO', format, *args)
    def debug(self, format, *args):
        import logging
        self.log(logging.DEBUG, 'DEBUG', format, *args)


@cached_function
def getLogger(name):
    return Logger(name)

def variable_to_index(var):
    P = var.parent()
    return P.gens().index(var)


def symmetries_to_permutation(syms):
    r"""
    EXAMPLES::

        sage: iv = VizingIdeal(nG=3, kG=2, nH=2, kH=1,
        ....:                  base_ring=QQ, order='deglex')
        sage: syms = iv.symmetries()
        sage: [tuple((v, variable_to_index(v)+1) for v in sym) for sym in syms]
        [((xG_0, 1), (xG_1, 2), (xG_2, 3)),
         ((eG_0_1, 6), (eG_0_2, 7), (eG_1_2, 8)),
         ((xH_0, 4), (xH_1, 5)),
         ((eH_0_1, 9),),
         ((xGH_0_0, 10),
          (xGH_0_1, 11),
          (xGH_1_0, 12),
          (xGH_1_1, 13),
          (xGH_2_0, 14),
          (xGH_2_1, 15))]
        sage: iv.polynomial_ring
        Multivariate Polynomial Ring in
            xG_0, xG_1, xG_2,
            xH_0, xH_1,
            eG_0_1, eG_0_2, eG_1_2,
            eH_0_1,
            xGH_0_0, xGH_0_1, xGH_1_0, xGH_1_1, xGH_2_0, xGH_2_1
            over Rational Field
        sage: [(i+1, p) for i, p in enumerate(symmetries_to_permutation(syms))]
        [(1, 2),
         (2, 3),
         (3, 1),
         (4, 5),
         (5, 4),
         (6, 7),
         (7, 8),
         (8, 6),
         (9, 9),
         (10, 11),
         (11, 12),
         (12, 13),
         (13, 14),
         (14, 15),
         (15, 10)]
    """
    P = syms[0][0].parent()
    permutation = Permutation(range(1, P.ngens()+1)) \
                  * Permutation([tuple(variable_to_index(P(v)) + 1
                                       for v in sym)
                                 for sym in syms])
    return tuple(permutation)


def symmStd(I, symmetries):
    r"""
    EXAMPLES::

        sage: P.<a,b,c,d> = PolynomialRing(QQ)
        sage: I = ideal([a + b + c + d,
        ....:            a*b + b*c + c*d + d*a,
        ....:            a*b*c + b*c*d + c*d*a + d*a*b,
        ....:            a*b*c*d - 1])
        sage: symmStd(I, [(a, b, c, d)])
        [a + b + c + d,
         b^2 + 2*b*d + d^2,
         b*c^2 + c^2*d - b*d^2 - d^3,
         b*c*d^2 + c^2*d^2 - b*d^3 + c*d^3 - d^4 - 1,
         b*d^4 + d^5 - b - d,
         c^3*d^2 + c^2*d^3 - c - d,
         c^2*d^4 + b*c - b*d + c*d - 2*d^2]

        sage: P.<x,y,z> = PolynomialRing(QQ)
        sage: I = ideal([x*y - y^2 + x*z,
        ....:            x*y + y*z - z^2,
        ....:            -x^2 + x*z + y*z])
        sage: symmStd(I, [(z, x, y)])
        [y^2 - x*z + y*z - z^2,
         x*y + y*z - z^2,
         x^2 - x*z - y*z,
         y*z^2 - z^3,
         x*z^2,
         z^4]

        sage: J = ideal([x^2*y^2 -z, x*y - 2*y + 3*z, x*y - 2*x + 3*z])
        sage: symmStd(J, [(x, y), (z,)])
        [x - y,
         12*y*z - 9*z^2 - 8*y + 13*z,
         y^2 - 2*y + 3*z,
         81*z^3 + 36*z^2 - 56*y + 115*z]
        sage: symmStd(J, [(x, y, z)])
        Traceback (most recent call last):
        ...
        RuntimeError: error in Singular function call 'symmStd':
        The input is no permutation of the ring-variables!!
        leaving symodstd.lib::symmStd
    """
    from sage.libs.singular.function import singular_function, lib as singular_lib
    singular_lib('symodstd.lib')
    f = singular_function('symmStd')

    pi = symmetries_to_permutation(symmetries)
    n = I.ring().ngens()
    if set(pi) != set(range(1, n+1)):
        raise ValueError('permutation {} not valid'.format(pi))

    return f(I, pi)


def unique(L):
    r"""
    EXAMPLES::

        sage: unique([2, 1, 3, 3, 1, 5, 2, 3])
        [2, 1, 3, 5]
    """
    result = []
    for ell in L:
        if ell not in result:
            result.append(ell)
    return result


load('infinite_polynomial_ring_sparse_exponents.py')

from functools import total_ordering

@total_ordering
class a(object):
    def __init__(self, i, j):
        if j < i:
            i, j = j, i
        self.i = i
        self.j = j
    def __repr__(self):
        return '{i}_{j}'.format(i=self.i, j=self.j)
    def __key__(self):
        return (self.j, self.i)
    def __hash__(self):
        return hash(self.__key__())
    def __eq__(self, other):
        return self.__key__() == other.__key__()
    def __lt__(self, other):
        return self.__key__() < other.__key__()
    def __neg__(self):
        return self.__class__(-self.i, -self.j)  # only needed for degrevlex

class OneGraph(object):
    r"""
    One of the graphs G or H.
    """

    def __init__(self, n, k, index, base_ring, order,
                 invert_xg=False,
                 fix_dominating_set=False):
        r"""
        INPUT:

        - ``invert_xg``
          - ``False``: x_g = 1 <==> g in dominating set
          - ``True``: x_g = 0 <==> g in dominating set
        """
        self.n = n
        self.k = k
        if index not in ('G', 'H'):
            raise ValueError
        self.index = index
        self.invert_xg = invert_xg
        self.fix_dominating_set = fix_dominating_set
        self.create_polynomial_ring(base_ring, order)

    def create_polynomial_ring(self, base_ring, order):
        r"""
        EXAMPLES::

            sage: G = OneGraph(3, 2, 'G', QQ, 'deglex')
            sage: G.polynomial_ring
            Multivariate Polynomial Ring
            in xG_0, xG_1, xG_2, eG_0_1, eG_0_2, eG_1_2 over Rational Field
        """
        n = self.n
        self.vertices_names = tuple(srange(n))
        self.edges_names = tuple((g, g_prime)
                                 for g in range(n)
                                 for g_prime in range(g+1, n))
        generator_names = (['x{}_{}'.format(self.index, g)
                            for g in self.vertices_names
                            if not self.fix_dominating_set]
                           + ['e{}_{}_{}'.format(self.index, g, g_prime)
                              for g, g_prime in self.edges_names])
        P = PolynomialRing(base_ring,
                           generator_names, len(generator_names),
                           order=order)
        gens = P.gens()
        if self.fix_dominating_set:
            self.vertices = tuple(1 for _ in range(self.k)) \
                            + tuple(0 for _ in range(self.n - self.k))
            if self.invert_xg:
                self.vertices = tuple(1 - xg for xg in self.vertices)
            self.vertices_variables = ()
            self.edges = gens
        else:
            self.vertices = gens[:n]
            self.vertices_variables = self.vertices
            self.edges = gens[n:]
        self.polynomial_ring = P

    def check_vertex(self, g):
        if g not in self.vertices_names:
            raise ValueError('{} is not a vertex in {}'.format(g, self.index))

    def vertex(self, g):
        self.check_vertex(g)
        return self.vertices[g]

    @staticmethod
    def _edge_index_(n, g, g_prime):
        assert g != g_prime
        if g > g_prime:
            g, g_prime = g_prime, g
        return g*n - (g+2)*(g+1)/2 + g_prime

    def edge(self, g, g_prime):
        r"""
        EXAMPLES::

            sage: G = OneGraph(4, 2, 'G', QQ, 'deglex')
            sage: G.polynomial_ring
            Multivariate Polynomial Ring
            in xG_0, xG_1, xG_2, xG_3,
               eG_0_1, eG_0_2, eG_0_3, eG_1_2, eG_1_3, eG_2_3
            over Rational Field
            sage: G.edge(0, 2), G.edge(1, 2), G.edge(2, 3)
            (eG_0_2, eG_1_2, eG_2_3)
            sage: G.edge(2, 0), G.edge(2, 1), G.edge(3, 2)
            (eG_0_2, eG_1_2, eG_2_3)
        """
        return self.edges[OneGraph._edge_index_(self.n, g, g_prime)]

    def equations_vertices(self):
        r"""
        1) x_g^2 - x_g = 0
        (ensure that x_g in {0, 1})
        """
        return tuple(xg^2 - xg for xg in self.vertices)

    def equations_edges(self):
        r"""
        2) e_ij^2 - e_ij = 0
        (ensure that e_ij in {0, 1})
        """
        return tuple(egg^2 - egg for egg in self.edges)

    def equations_dominating_set_size(self):
        if self.invert_xg:
            return self.equations_dominating_set_size_x0()
        else:
            return self.equations_dominating_set_size_x1()

    def equations_dominating_set_size_x1(self):
        r"""
        3) sum(x_i) = k
        (ensure that the dominating set has size k)
        x_g = 1 <==> g in dominating set
        """
        n = self.n
        k = self.k
        return (sum(xg for xg in self.vertices) - k,)

    def equations_dominating_set_size_x0(self):
        r"""
        3) sum(x_i) = n-k
        (ensure that the dominating set has size k)
        x_g = 0 <==> g in dominating set
        """
        n = self.n
        k = self.k
        return (sum(xg for xg in self.vertices) - (n-k),)

    def equations_dominating_set(self):
        if self.invert_xg:
            return self.equations_dominating_set_x0()
        else:
            return self.equations_dominating_set_x1()

    def equations_dominating_set_x1(self):
        r"""
        4) (1-x_i)*prod(1 - e_ij*x_j) = 0
        x_g = 1 <==> g in dominating set
        """
        x = self.vertex
        e = self.edge

        return tuple((1-x(g)) * prod(1 - e(g, g_prime) * x(g_prime)
                                     for g_prime in self.vertices_names
                                     if g != g_prime)
                     for g in self.vertices_names)

    def equations_dominating_set_x0(self):
        r"""
        4) x_i*prod(1 - e_ij*(1-x_j)) = 0
        x_g = 0 <==> g in dominating set
        """
        x = self.vertex
        e = self.edge

        return tuple(x(g) * prod(1 - e(g, g_prime) * (1-x(g_prime))
                                     for g_prime in self.vertices_names
                                     if g != g_prime)
                     for g in self.vertices_names)

    def equations_cover(self):
        r"""
        5) prod(...)
        """
        e = self.edge

        return tuple(prod(sum(e(g, g_prime) for g in S)
                          for g_prime in self.vertices_names
                          if g_prime not in S)
                     for S in Subsets(self.vertices_names, self.k-1))

    def equations_all(self, P=None):
        r"""
        EXAMPLES::

            sage: G = OneGraph(3, 2, 'G', QQ, 'deglex')
            sage: G.equations_all()
            (xG_0^2 - xG_0,
             xG_1^2 - xG_1,
             xG_2^2 - xG_2,
             eG_0_1^2 - eG_0_1,
             eG_0_2^2 - eG_0_2,
             eG_1_2^2 - eG_1_2,
             xG_0 + xG_1 + xG_2 - 2,
             -xG_0*xG_1*xG_2*eG_0_1*eG_0_2 + xG_1*xG_2*eG_0_1*eG_0_2 + xG_0*xG_1*eG_0_1 + xG_0*xG_2*eG_0_2 - xG_1*eG_0_1 - xG_2*eG_0_2 - xG_0 + 1,
             -xG_0*xG_1*xG_2*eG_0_1*eG_1_2 + xG_0*xG_2*eG_0_1*eG_1_2 + xG_0*xG_1*eG_0_1 + xG_1*xG_2*eG_1_2 - xG_0*eG_0_1 - xG_2*eG_1_2 - xG_1 + 1,
             -xG_0*xG_1*xG_2*eG_0_2*eG_1_2 + xG_0*xG_1*eG_0_2*eG_1_2 + xG_0*xG_2*eG_0_2 + xG_1*xG_2*eG_1_2 - xG_0*eG_0_2 - xG_1*eG_1_2 - xG_2 + 1,
             eG_0_1*eG_0_2,
             eG_0_1*eG_1_2,
             eG_0_2*eG_1_2)

            sage: G = OneGraph(4, 3, 'G', QQ, 'deglex', fix_dominating_set=True)
            sage: G.equations_all()
            (eG_0_1^2 - eG_0_1,
             eG_0_2^2 - eG_0_2,
             eG_0_3^2 - eG_0_3,
             eG_1_2^2 - eG_1_2,
             eG_1_3^2 - eG_1_3,
             eG_2_3^2 - eG_2_3,
             -eG_0_3*eG_1_3*eG_2_3 + eG_0_3*eG_1_3 + eG_0_3*eG_2_3 + eG_1_3*eG_2_3 - eG_0_3 - eG_1_3 - eG_2_3 + 1,
             eG_0_2*eG_0_3 + eG_0_2*eG_1_3 + eG_0_3*eG_1_2 + eG_1_2*eG_1_3,
             eG_0_1*eG_0_3 + eG_0_1*eG_2_3 + eG_0_3*eG_1_2 + eG_1_2*eG_2_3,
             eG_0_1*eG_0_2 + eG_0_1*eG_2_3 + eG_0_2*eG_1_3 + eG_1_3*eG_2_3,
             eG_0_1*eG_1_3 + eG_0_1*eG_2_3 + eG_0_2*eG_1_3 + eG_0_2*eG_2_3,
             eG_0_1*eG_1_2 + eG_0_1*eG_2_3 + eG_0_3*eG_1_2 + eG_0_3*eG_2_3,
             eG_0_2*eG_1_2 + eG_0_2*eG_1_3 + eG_0_3*eG_1_2 + eG_0_3*eG_1_3)
        """
        eqns = (self.equations_vertices()
                + self.equations_edges()
                + self.equations_dominating_set_size()
                + self.equations_dominating_set()
                + self.equations_cover())
        eqns = tuple(eqn for eqn in eqns if eqn != 0)
        if P is None:
            return eqns
        else:
            # convert
            return tuple(P(eqn) for eqn in eqns)

    def symmetries(self, P=None):
        r"""
        EXAMPLES::

            sage: G = OneGraph(3, 2, 'G', QQ, 'deglex')
            sage: G.symmetries()
            [(xG_0, xG_1, xG_2), (eG_0_1, eG_0_2, eG_1_2)]

            sage: G = OneGraph(4, 3, 'G', QQ, 'deglex', fix_dominating_set=True)
            sage: G.symmetries()
            [(eG_0_1, eG_0_2, eG_0_3, eG_1_2, eG_1_3, eG_2_3)]
        """
        if self.fix_dominating_set:
            syms = [self.edges]
        else:
            syms = [self.vertices, self.edges]
        if P is None:
            return syms
        else:
            #convert
            return [tuple(P(v) for v in sym) for sym in syms]


class BoxGraph(object):

    def __init__(self, G, H, invert_xg=False, **kwds):
        self.G = G
        self.H = H
        self.index = G.index + H.index
        P = self.G.polynomial_ring
        self.invert_xg = invert_xg
        self.create_polynomial_ring(P.base_ring(), P.term_order().name(), **kwds)

    def create_polynomial_ring(self, base_ring, order,
                               additional_variables=None):
        if additional_variables is None:
            additional_variables = 0
        nG = self.G.n
        nH = self.H.n
        xG = self.G.vertices_variables
        xH = self.H.vertices_variables
        eG = self.G.edges
        eH = self.H.edges

        generator_names = ([str(xg) for xg in xG]
                           + [str(xh) for xh in xH]
                           + [str(eg) for eg in eG]
                           + [str(eh) for eh in eH]
                           + ['x{}_{}_{}'.format(self.index, g, h)
                              for g in self.G.vertices_names
                              for h in self.H.vertices_names])
        generator_names += ['alpha_{}'.format(i)
                            for i in range(additional_variables)]
        P = PolynomialRing(base_ring,
                           generator_names, len(generator_names),
                           order=order)

        gens = iter(P.gens())
        def next_gens(m):
            return tuple(next(gens) for _ in range(m))
        self.verticesG_variables = next_gens(len(xG))
        self.verticesH_variables = next_gens(len(xH))
        self.edgesG = next_gens(binomial(nG, 2))
        self.edgesH = next_gens(binomial(nH, 2))
        self.verticesGH = next_gens(nG * nH)
        self.additional_variables = next_gens(additional_variables)
        self.polynomial_ring = P

    def vertexGH(self, g, h):
        self.G.check_vertex(g)
        self.H.check_vertex(h)
        nH = self.H.n
        return self.verticesGH[g*nH + h]

    def edgeG(self, g, g_prime):
        self.G.check_vertex(g)
        self.G.check_vertex(g_prime)
        return self.edgesG[OneGraph._edge_index_(self.G.n, g, g_prime)]

    def edgeH(self, h, h_prime):
        self.H.check_vertex(h)
        self.H.check_vertex(h_prime)
        return self.edgesH[OneGraph._edge_index_(self.H.n, h, h_prime)]

    def equations_vertices(self):
        r"""
        6) x_g^2 - x_g = 0
        """
        return tuple(xgh^2 - xgh for xgh in self.verticesGH)

    def equations_dominating_set(self):
        if self.invert_xg:
            return self.equations_dominating_set_x0()
        else:
            return self.equations_dominating_set_x1()

    def equations_dominating_set_x1(self):
        r"""
        7) x_gh*prod(...)*prod(...) = 0
        x_gh = 1 <==> gh in dominating set
        """
        x = self.vertexGH
        eG = self.edgeG
        eH = self.edgeH

        return tuple((1 - x(g, h))
                     * prod(1 - eG(g, g_prime) * x(g_prime, h)
                            for g_prime in self.G.vertices_names
                            if g != g_prime)
                     * prod(1 - eH(h, h_prime) * x(g, h_prime)
                            for h_prime in self.H.vertices_names
                            if h != h_prime)
                     for g in self.G.vertices_names
                     for h in self.H.vertices_names)

    def equations_dominating_set_x0(self):
        r"""
        7) x_gh*prod(...)*prod(...) = 0
        x_gh = 0 <==> gh in dominating set
        """
        x = self.vertexGH
        eG = self.edgeG
        eH = self.edgeH

        return tuple(x(g, h)
                     * prod(1 - eG(g, g_prime) * (1 - x(g_prime, h))
                            for g_prime in self.G.vertices_names
                            if g != g_prime)
                     * prod(1 - eH(h, h_prime) * (1 - x(g, h_prime))
                            for h_prime in self.H.vertices_names
                            if h != h_prime)
                     for g in self.G.vertices_names
                     for h in self.H.vertices_names)

    def equations_dominating_set_size(self, **kwds):
        if self.invert_xg:
            return self.equations_dominating_set_size_x0(**kwds)
        else:
            return self.equations_dominating_set_size_x1(**kwds)

    def equations_dominating_set_size_x1(self, k=None):
        if k is None:
            k = self.G.k * self.H.k - 1
        x = self.vertexGH
        return (sum(x(g, h)
                    for g in self.G.vertices_names
                    for h in self.H.vertices_names) - k,)

    def equations_all(self, P=None, with_size=False, k=None):
        r"""
        EXAMPLES::

            sage: G = OneGraph(2, 1, 'G', QQ, 'deglex')
            sage: H = OneGraph(2, 1, 'H', QQ, 'deglex')
            sage: B = BoxGraph(G, H)
            sage: B.equations_all()
            (xGH_0_0^2 - xGH_0_0,
             xGH_0_1^2 - xGH_0_1,
             xGH_1_0^2 - xGH_1_0,
             xGH_1_1^2 - xGH_1_1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_0 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0 + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_0_0*xGH_0_1 - eG_0_1*xGH_1_0 - eH_0_1*xGH_0_1 - xGH_0_0 + 1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1 + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_0_0*xGH_0_1 - eG_0_1*xGH_1_1 - eH_0_1*xGH_0_0 - xGH_0_1 + 1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1 + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_1_0*xGH_1_1 - eG_0_1*xGH_0_0 - eH_0_1*xGH_1_1 - xGH_1_0 + 1,
             -eG_0_1*eH_0_1*xGH_0_1*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0 + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_1_0*xGH_1_1 - eG_0_1*xGH_0_1 - eH_0_1*xGH_1_0 - xGH_1_1 + 1)

            sage: G = OneGraph(3, 2, 'G', QQ, 'deglex', fix_dominating_set=True)
            sage: H = OneGraph(2, 1, 'H', QQ, 'deglex', fix_dominating_set=True)
            sage: B = BoxGraph(G, H)
            sage: B.equations_all()
            (xGH_0_0^2 - xGH_0_0,
             xGH_0_1^2 - xGH_0_1,
             xGH_1_0^2 - xGH_1_0,
             xGH_1_1^2 - xGH_1_1,
             xGH_2_0^2 - xGH_2_0,
             xGH_2_1^2 - xGH_2_1,
             eG_0_1*eG_0_2*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_0*xGH_2_0 - eG_0_1*eG_0_2*eH_0_1*xGH_0_1*xGH_1_0*xGH_2_0 - eG_0_1*eG_0_2*xGH_0_0*xGH_1_0*xGH_2_0 - eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_0 - eG_0_2*eH_0_1*xGH_0_0*xGH_0_1*xGH_2_0 + eG_0_1*eG_0_2*xGH_1_0*xGH_2_0 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0 + eG_0_2*eH_0_1*xGH_0_1*xGH_2_0 + eG_0_1*xGH_0_0*xGH_1_0 + eG_0_2*xGH_0_0*xGH_2_0 + eH_0_1*xGH_0_0*xGH_0_1 - eG_0_1*xGH_1_0 - eG_0_2*xGH_2_0 - eH_0_1*xGH_0_1 - xGH_0_0 + 1,
             eG_0_1*eG_0_2*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_1*xGH_2_1 - eG_0_1*eG_0_2*eH_0_1*xGH_0_0*xGH_1_1*xGH_2_1 - eG_0_1*eG_0_2*xGH_0_1*xGH_1_1*xGH_2_1 - eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_1 - eG_0_2*eH_0_1*xGH_0_0*xGH_0_1*xGH_2_1 + eG_0_1*eG_0_2*xGH_1_1*xGH_2_1 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1 + eG_0_2*eH_0_1*xGH_0_0*xGH_2_1 + eG_0_1*xGH_0_1*xGH_1_1 + eG_0_2*xGH_0_1*xGH_2_1 + eH_0_1*xGH_0_0*xGH_0_1 - eG_0_1*xGH_1_1 - eG_0_2*xGH_2_1 - eH_0_1*xGH_0_0 - xGH_0_1 + 1,
             eG_0_1*eG_1_2*eH_0_1*xGH_0_0*xGH_1_0*xGH_1_1*xGH_2_0 - eG_0_1*eG_1_2*eH_0_1*xGH_0_0*xGH_1_1*xGH_2_0 - eG_0_1*eG_1_2*xGH_0_0*xGH_1_0*xGH_2_0 - eG_0_1*eH_0_1*xGH_0_0*xGH_1_0*xGH_1_1 - eG_1_2*eH_0_1*xGH_1_0*xGH_1_1*xGH_2_0 + eG_0_1*eG_1_2*xGH_0_0*xGH_2_0 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1 + eG_1_2*eH_0_1*xGH_1_1*xGH_2_0 + eG_0_1*xGH_0_0*xGH_1_0 + eG_1_2*xGH_1_0*xGH_2_0 + eH_0_1*xGH_1_0*xGH_1_1 - eG_0_1*xGH_0_0 - eG_1_2*xGH_2_0 - eH_0_1*xGH_1_1 - xGH_1_0 + 1,
             eG_0_1*eG_1_2*eH_0_1*xGH_0_1*xGH_1_0*xGH_1_1*xGH_2_1 - eG_0_1*eG_1_2*eH_0_1*xGH_0_1*xGH_1_0*xGH_2_1 - eG_0_1*eG_1_2*xGH_0_1*xGH_1_1*xGH_2_1 - eG_0_1*eH_0_1*xGH_0_1*xGH_1_0*xGH_1_1 - eG_1_2*eH_0_1*xGH_1_0*xGH_1_1*xGH_2_1 + eG_0_1*eG_1_2*xGH_0_1*xGH_2_1 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0 + eG_1_2*eH_0_1*xGH_1_0*xGH_2_1 + eG_0_1*xGH_0_1*xGH_1_1 + eG_1_2*xGH_1_1*xGH_2_1 + eH_0_1*xGH_1_0*xGH_1_1 - eG_0_1*xGH_0_1 - eG_1_2*xGH_2_1 - eH_0_1*xGH_1_0 - xGH_1_1 + 1,
             eG_0_2*eG_1_2*eH_0_1*xGH_0_0*xGH_1_0*xGH_2_0*xGH_2_1 - eG_0_2*eG_1_2*eH_0_1*xGH_0_0*xGH_1_0*xGH_2_1 - eG_0_2*eG_1_2*xGH_0_0*xGH_1_0*xGH_2_0 - eG_0_2*eH_0_1*xGH_0_0*xGH_2_0*xGH_2_1 - eG_1_2*eH_0_1*xGH_1_0*xGH_2_0*xGH_2_1 + eG_0_2*eG_1_2*xGH_0_0*xGH_1_0 + eG_0_2*eH_0_1*xGH_0_0*xGH_2_1 + eG_1_2*eH_0_1*xGH_1_0*xGH_2_1 + eG_0_2*xGH_0_0*xGH_2_0 + eG_1_2*xGH_1_0*xGH_2_0 + eH_0_1*xGH_2_0*xGH_2_1 - eG_0_2*xGH_0_0 - eG_1_2*xGH_1_0 - eH_0_1*xGH_2_1 - xGH_2_0 + 1,
             eG_0_2*eG_1_2*eH_0_1*xGH_0_1*xGH_1_1*xGH_2_0*xGH_2_1 - eG_0_2*eG_1_2*eH_0_1*xGH_0_1*xGH_1_1*xGH_2_0 - eG_0_2*eG_1_2*xGH_0_1*xGH_1_1*xGH_2_1 - eG_0_2*eH_0_1*xGH_0_1*xGH_2_0*xGH_2_1 - eG_1_2*eH_0_1*xGH_1_1*xGH_2_0*xGH_2_1 + eG_0_2*eG_1_2*xGH_0_1*xGH_1_1 + eG_0_2*eH_0_1*xGH_0_1*xGH_2_0 + eG_1_2*eH_0_1*xGH_1_1*xGH_2_0 + eG_0_2*xGH_0_1*xGH_2_1 + eG_1_2*xGH_1_1*xGH_2_1 + eH_0_1*xGH_2_0*xGH_2_1 - eG_0_2*xGH_0_1 - eG_1_2*xGH_1_1 - eH_0_1*xGH_2_0 - xGH_2_1 + 1)
        """
        eqns = (self.equations_vertices()
                + self.equations_dominating_set())
        if with_size:
            eqns += self.equations_dominating_set_size(k=k)
        eqns = tuple(eqn for eqn in eqns if eqn != 0)
        if P is None:
            return eqns
        else:
            # convert
            return tuple(P(eqn) for eqn in eqns)

    def symmetries(self, P=None):
        r"""
        EXAMPLES::

            sage: G = OneGraph(3, 2, 'G', QQ, 'deglex')
            sage: H = OneGraph(2, 1, 'H', QQ, 'deglex')
            sage: B = BoxGraph(G, H)
            sage: B.symmetries()
            [(eG_0_1, eG_0_2, eG_1_2),
             (eH_0_1,),
             (xGH_0_0, xGH_0_1, xGH_1_0, xGH_1_1, xGH_2_0, xGH_2_1)]

            sage: G = OneGraph(3, 2, 'G', QQ, 'deglex', fix_dominating_set=True)
            sage: H = OneGraph(2, 1, 'H', QQ, 'deglex', fix_dominating_set=True)
            sage: B = BoxGraph(G, H)
            sage: B.symmetries()
            [(eG_0_1, eG_0_2, eG_1_2),
             (eH_0_1,),
             (xGH_0_0, xGH_0_1, xGH_1_0, xGH_1_1, xGH_2_0, xGH_2_1)]
        """
        x = self.vertexGH
        eG = self.edgeG
        eH = self.edgeH
        VG = self.G.vertices_names
        VH = self.H.vertices_names

        cycles = []
        done = set()
        for edge_h in self.H.edges_names[:1]:
            if edge_h in done:
                continue
            h_0, h_prime_0 = edge_h
            i_h_0 = VH.index(h_0)
            i_h_prime_0 = VH.index(h_prime_0)
            cycle_eH = []
            cycle_x = tuple((g, []) for g in VG)
            for j in range(len(VH)):
                h = VH[(i_h_0 + j) % len(VH)]
                h_prime = VH[(i_h_prime_0 + j) % len(VH)]
                edge = (h, h_prime)
                done.add(edge)
                cycle_eH.append(eH(h, h_prime))
                for g, cycle in cycle_x:
                    cycle.append(x(g, h_prime))
            cycles.append(tuple(cycle_eH))
            cycles.extend(tuple(cycle) for g, cycle in cycle_x)
        return cycles
        
        x = self.vertexGH
        syms = [self.edgesG, self.edgesH] #\
               #+ [tuple(x(g, h) for h in self.H.vertices_names)
                #  for g in self.G.vertices_names]
        if P is None:
            return syms
        else:
            return [tuple(P(v) for v in sy) for sy in syms]

    def xS(self, g, h):
        r"""
        EXAMPLES::

            sage: G = OneGraph(3, 2, 'G', QQ, 'deglex')
            sage: H = OneGraph(3, 2, 'H', QQ, 'deglex')
            sage: B = BoxGraph(G, H)
            sage: B.xS(0, [1])
            xGH_0_0 + xGH_0_1 + xGH_0_2
            sage: B.xS(0, [1,1])
            2*xGH_0_0*xGH_0_1 + 2*xGH_0_0*xGH_0_2 + 2*xGH_0_1*xGH_0_2
            sage: B.xS(0, [1,1,1])
            6*xGH_0_0*xGH_0_1*xGH_0_2
        """
        from itertools import product

        if isinstance(g, (list, tuple)):
            lg = len(g)
            pG = iter(p for p in product(self.G.vertices_names, repeat=lg)
                      if len(set(p)) == lg)
            eG = g
        else:
            pG = [(g,)]
            eG = [1]

        if isinstance(h, (list, tuple)):
            lh = len(h)
            pH = iter(p for p in product(self.H.vertices_names, repeat=lh)
                      if len(set(p)) == lh)
            eH = h
        else:
            pH = [(h,)]
            eH = [1]

        x = self.vertexGH
        return sum(prod(x(gg, hh)
                        for gg, eg in zip(pg, eG)
                        for hh, eh in zip(ph, eH))
                   for pg in pG
                   for ph in pH)


class VizingIdeal(object):

    def __init__(self, nG, kG, nH, kH, base_ring, order,
                 invert_xg=False,
                 fix_dominating_set=False,
                 additional_variables=None):
        self.invert_xg = invert_xg

        self.G = OneGraph(nG, kG, 'G', base_ring, order,
                          invert_xg=invert_xg,
                          fix_dominating_set=fix_dominating_set)
        self.H = OneGraph(nH, kH, 'H', base_ring, order,
                          invert_xg=invert_xg,
                          fix_dominating_set=fix_dominating_set)
        self.GH = BoxGraph(self.G, self.H,
                           invert_xg=invert_xg,
                           additional_variables=additional_variables)
        self.polynomial_ring = self.GH.polynomial_ring

    def __repr__(self):
        return 'vizing ideal with nG={}, kG={}, nH={}, kH={}'.format(
            self.G.n, self.G.k, self.H.n, self.H.k)

    __str__ = __repr__

    def equations(self, uniquify=True, **kwds):
        r"""
        EXAMPLES::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex')
            sage: iv.equations()
            (xG_0^2 - xG_0,
             xG_1^2 - xG_1,
             eG_0_1^2 - eG_0_1,
             xG_0 + xG_1 - 2,
             xG_0*xG_1*eG_0_1 - xG_1*eG_0_1 - xG_0 + 1,
             xG_0*xG_1*eG_0_1 - xG_0*eG_0_1 - xG_1 + 1,
             eG_0_1,
             xH_0^2 - xH_0,
             xH_1^2 - xH_1,
             eH_0_1^2 - eH_0_1,
             xH_0 + xH_1 - 2,
             xH_0*xH_1*eH_0_1 - xH_1*eH_0_1 - xH_0 + 1,
             xH_0*xH_1*eH_0_1 - xH_0*eH_0_1 - xH_1 + 1,
             eH_0_1,
             xGH_0_0^2 - xGH_0_0,
             xGH_0_1^2 - xGH_0_1,
             xGH_1_0^2 - xGH_1_0,
             xGH_1_1^2 - xGH_1_1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_0 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0
             + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_0_0*xGH_0_1
             - eG_0_1*xGH_1_0 - eH_0_1*xGH_0_1 - xGH_0_0 + 1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1
             + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_0_0*xGH_0_1
             - eG_0_1*xGH_1_1 - eH_0_1*xGH_0_0 - xGH_0_1 + 1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1
             + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_1_0*xGH_1_1
             - eG_0_1*xGH_0_0 - eH_0_1*xGH_1_1 - xGH_1_0 + 1,
             -eG_0_1*eH_0_1*xGH_0_1*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0
             + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_1_0*xGH_1_1
             - eG_0_1*xGH_0_1 - eH_0_1*xGH_1_0 - xGH_1_1 + 1)

        ::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  invert_xg=True)
            sage: iv.equations()
            (xG_0^2 - xG_0,
             xG_1^2 - xG_1,
             eG_0_1^2 - eG_0_1,
             xG_0 + xG_1,
             xG_0*xG_1*eG_0_1 - xG_0*eG_0_1 + xG_0,
             xG_0*xG_1*eG_0_1 - xG_1*eG_0_1 + xG_1,
             eG_0_1,
             xH_0^2 - xH_0,
             xH_1^2 - xH_1,
             eH_0_1^2 - eH_0_1,
             xH_0 + xH_1,
             xH_0*xH_1*eH_0_1 - xH_0*eH_0_1 + xH_0,
             xH_0*xH_1*eH_0_1 - xH_1*eH_0_1 + xH_1,
             eH_0_1,
             xGH_0_0^2 - xGH_0_0,
             xGH_0_1^2 - xGH_0_1,
             xGH_1_0^2 - xGH_1_0,
             xGH_1_1^2 - xGH_1_1,
             eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_0 - eG_0_1*eH_0_1*xGH_0_0*xGH_0_1
               - eG_0_1*eH_0_1*xGH_0_0*xGH_1_0 + eG_0_1*eH_0_1*xGH_0_0
               + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_0_0*xGH_0_1 
               - eG_0_1*xGH_0_0 - eH_0_1*xGH_0_0 + xGH_0_0,
             eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_1 - eG_0_1*eH_0_1*xGH_0_0*xGH_0_1
               - eG_0_1*eH_0_1*xGH_0_1*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_1
               + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_0_0*xGH_0_1
               - eG_0_1*xGH_0_1 - eH_0_1*xGH_0_1 + xGH_0_1,
             eG_0_1*eH_0_1*xGH_0_0*xGH_1_0*xGH_1_1 - eG_0_1*eH_0_1*xGH_0_0*xGH_1_0
               - eG_0_1*eH_0_1*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_1_0
               + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_1_0*xGH_1_1
               - eG_0_1*xGH_1_0 - eH_0_1*xGH_1_0 + xGH_1_0,
             eG_0_1*eH_0_1*xGH_0_1*xGH_1_0*xGH_1_1 - eG_0_1*eH_0_1*xGH_0_1*xGH_1_1
               - eG_0_1*eH_0_1*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_1_1
               + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_1_0*xGH_1_1
               - eG_0_1*xGH_1_1 - eH_0_1*xGH_1_1 + xGH_1_1)

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  fix_dominating_set=True)
            sage: iv.equations()
            (eG_0_1^2 - eG_0_1,
             eG_0_1,
             eH_0_1^2 - eH_0_1,
             eH_0_1,
             xGH_0_0^2 - xGH_0_0,
             xGH_0_1^2 - xGH_0_1,
             xGH_1_0^2 - xGH_1_0,
             xGH_1_1^2 - xGH_1_1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_0 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0
             + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_0_0*xGH_0_1
             - eG_0_1*xGH_1_0 - eH_0_1*xGH_0_1 - xGH_0_0 + 1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1
             + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_0_0*xGH_0_1
             - eG_0_1*xGH_1_1 - eH_0_1*xGH_0_0 - xGH_0_1 + 1,
             -eG_0_1*eH_0_1*xGH_0_0*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_0*xGH_1_1
             + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_1_0*xGH_1_1
             - eG_0_1*xGH_0_0 - eH_0_1*xGH_1_1 - xGH_1_0 + 1,
             -eG_0_1*eH_0_1*xGH_0_1*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_1*xGH_1_0
             + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_1_0*xGH_1_1
             - eG_0_1*xGH_0_1 - eH_0_1*xGH_1_0 - xGH_1_1 + 1)

        ::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  invert_xg=True,
            ....:                  fix_dominating_set=True)
            sage: iv.equations()
            (eG_0_1^2 - eG_0_1,
             eG_0_1,
             eH_0_1^2 - eH_0_1,
             eH_0_1,
             xGH_0_0^2 - xGH_0_0,
             xGH_0_1^2 - xGH_0_1,
             xGH_1_0^2 - xGH_1_0,
             xGH_1_1^2 - xGH_1_1,
             eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_0 - eG_0_1*eH_0_1*xGH_0_0*xGH_0_1
               - eG_0_1*eH_0_1*xGH_0_0*xGH_1_0 + eG_0_1*eH_0_1*xGH_0_0
               + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_0_0*xGH_0_1 
               - eG_0_1*xGH_0_0 - eH_0_1*xGH_0_0 + xGH_0_0,
             eG_0_1*eH_0_1*xGH_0_0*xGH_0_1*xGH_1_1 - eG_0_1*eH_0_1*xGH_0_0*xGH_0_1
               - eG_0_1*eH_0_1*xGH_0_1*xGH_1_1 + eG_0_1*eH_0_1*xGH_0_1
               + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_0_0*xGH_0_1
               - eG_0_1*xGH_0_1 - eH_0_1*xGH_0_1 + xGH_0_1,
             eG_0_1*eH_0_1*xGH_0_0*xGH_1_0*xGH_1_1 - eG_0_1*eH_0_1*xGH_0_0*xGH_1_0
               - eG_0_1*eH_0_1*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_1_0
               + eG_0_1*xGH_0_0*xGH_1_0 + eH_0_1*xGH_1_0*xGH_1_1
               - eG_0_1*xGH_1_0 - eH_0_1*xGH_1_0 + xGH_1_0,
             eG_0_1*eH_0_1*xGH_0_1*xGH_1_0*xGH_1_1 - eG_0_1*eH_0_1*xGH_0_1*xGH_1_1
               - eG_0_1*eH_0_1*xGH_1_0*xGH_1_1 + eG_0_1*eH_0_1*xGH_1_1
               + eG_0_1*xGH_0_1*xGH_1_1 + eH_0_1*xGH_1_0*xGH_1_1
               - eG_0_1*xGH_1_1 - eH_0_1*xGH_1_1 + xGH_1_1)
        """
        P = self.polynomial_ring
        eqns = (self.G.equations_all(P)
                + self.H.equations_all(P)
                + self.GH.equations_all(P, **kwds))
        if uniquify:
            eqns = tuple(unique(eqns))
        return eqns

    def symmetries(self):
        r"""
        EXAMPLES::

            sage: iv = VizingIdeal(nG=3, kG=2, nH=2, kH=1,
            ....:                  base_ring=QQ, order='deglex')
            sage: iv.symmetries()
            [(xG_0, xG_1, xG_2),
             (eG_0_1, eG_0_2, eG_1_2),
             (xH_0, xH_1),
             (eH_0_1,),
             (xGH_0_0, xGH_0_1, xGH_1_0, xGH_1_1, xGH_2_0, xGH_2_1)]

            sage: iv = VizingIdeal(nG=3, kG=2, nH=2, kH=1,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  fix_dominating_set=True)
            sage: iv.symmetries()
            [(eG_0_1, eG_0_2, eG_1_2),
             (eH_0_1,),
             (xGH_0_0, xGH_0_1, xGH_1_0, xGH_1_1, xGH_2_0, xGH_2_1)]
        """
        P = self.polynomial_ring
        return self.GH.symmetries(P)
        
        #P = self.polynomial_ring
        #syms = (self.G.symmetries(P)
        #        + self.H.symmetries(P)
        #        + self.GH.symmetries(P))
        #return unique(syms)

    @property
    def polynomial_ring_nulla(self):
        P = self.polynomial_ring
        return P.change_ring(names=['x_{}'.format(i)
                                    for i, _ in enumerate(P.gens())])

    def nulla_variable_mapping(self, reverse=False):
        if reverse:
            return {b: a for a, b in zip(self.polynomial_ring.gens(),
                                         self.polynomial_ring_nulla.gens())}
        else:
            return {a: b for a, b in zip(self.polynomial_ring.gens(),
                                         self.polynomial_ring_nulla.gens())}

    def equations_nulla(self, return_string=False, **kwds):
        r"""
        EXAMPLES::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex')
            sage: iv.equations_nulla()
            (x_0^2 - x_0,
             x_1^2 - x_1,
             x_4^2 - x_4,
             x_0 + x_1 - 2,
             x_0*x_1*x_4 - x_1*x_4 - x_0 + 1,
             x_0*x_1*x_4 - x_0*x_4 - x_1 + 1,
             x_4,
             x_2^2 - x_2,
             x_3^2 - x_3,
             x_5^2 - x_5,
             x_2 + x_3 - 2,
             x_2*x_3*x_5 - x_3*x_5 - x_2 + 1,
             x_2*x_3*x_5 - x_2*x_5 - x_3 + 1,
             x_5,
             x_6^2 - x_6,
             x_7^2 - x_7,
             x_8^2 - x_8,
             x_9^2 - x_9,
             -x_4*x_5*x_6*x_7*x_8 + x_4*x_5*x_7*x_8 + x_4*x_6*x_8 + x_5*x_6*x_7 - x_4*x_8 - x_5*x_7 - x_6 + 1,
             -x_4*x_5*x_6*x_7*x_9 + x_4*x_5*x_6*x_9 + x_4*x_7*x_9 + x_5*x_6*x_7 - x_4*x_9 - x_5*x_6 - x_7 + 1,
             -x_4*x_5*x_6*x_8*x_9 + x_4*x_5*x_6*x_9 + x_4*x_6*x_8 + x_5*x_8*x_9 - x_4*x_6 - x_5*x_9 - x_8 + 1,
             -x_4*x_5*x_7*x_8*x_9 + x_4*x_5*x_7*x_8 + x_4*x_7*x_9 + x_5*x_8*x_9 - x_4*x_7 - x_5*x_8 - x_9 + 1)
            sage: print(iv.equations_nulla(return_string=True))
            x[0]^2 - x[0]
            x[1]^2 - x[1]
            x[4]^2 - x[4]
            x[0] + x[1] - 2
            x[0]*x[1]*x[4] - x[1]*x[4] - x[0] + 1
            x[0]*x[1]*x[4] - x[0]*x[4] - x[1] + 1
            x[4]
            x[2]^2 - x[2]
            x[3]^2 - x[3]
            x[5]^2 - x[5]
            x[2] + x[3] - 2
            x[2]*x[3]*x[5] - x[3]*x[5] - x[2] + 1
            x[2]*x[3]*x[5] - x[2]*x[5] - x[3] + 1
            x[5]
            x[6]^2 - x[6]
            x[7]^2 - x[7]
            x[8]^2 - x[8]
            x[9]^2 - x[9]
            -x[4]*x[5]*x[6]*x[7]*x[8] + x[4]*x[5]*x[7]*x[8] + x[4]*x[6]*x[8] + x[5]*x[6]*x[7] - x[4]*x[8] - x[5]*x[7] - x[6] + 1
            -x[4]*x[5]*x[6]*x[7]*x[9] + x[4]*x[5]*x[6]*x[9] + x[4]*x[7]*x[9] + x[5]*x[6]*x[7] - x[4]*x[9] - x[5]*x[6] - x[7] + 1
            -x[4]*x[5]*x[6]*x[8]*x[9] + x[4]*x[5]*x[6]*x[9] + x[4]*x[6]*x[8] + x[5]*x[8]*x[9] - x[4]*x[6] - x[5]*x[9] - x[8] + 1
            -x[4]*x[5]*x[7]*x[8]*x[9] + x[4]*x[5]*x[7]*x[8] + x[4]*x[7]*x[9] + x[5]*x[8]*x[9] - x[4]*x[7] - x[5]*x[8] - x[9] + 1
        """
        eqns = self.equations(**kwds)
        P = self.polynomial_ring_nulla
        eqns = tuple(eqn(P.gens()) for eqn in eqns)
        if return_string:
            import re
            return '\n'.join(re.sub(r'_([0-9]+)', r'[\1]', str(eqn))
                             for eqn in eqns)
        else:
            return eqns

    @cached_method(do_pickle=True)
    def ideal(self, **kwds):
        P = self.polynomial_ring
        return P.ideal(self.equations(**kwds))

    def groebner_basis(self, use_symmetries=False, **kwds):
        r"""
        EXAMPLES::

            sage: iv = VizingIdeal(nG=2, kG=1, nH=2, kH=1,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  fix_dominating_set=True)
            sage: iv.groebner_basis()
        """
        I = self.ideal(**kwds)
        GB = I.groebner_basis(algorithm='libsingular:slimgb')
        I.groebner_basis.set_cache(GB)
        return GB

    def groebner_basis(self, use_symmetries=False, **kwds):
        r"""
        EXAMPLES::

            sage: iv = VizingIdeal(nG=2, kG=1, nH=2, kH=1,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  fix_dominating_set=True)
            sage: iv.groebner_basis(use_symmetries=True)
            [eH_0_1 - 1, eG_0_1 - 1, xGH_1_1^2 - xGH_1_1, xGH_1_0^2 - xGH_1_0, xGH_0_1^2 - xGH_0_1, xGH_0_0^2 - xGH_0_0, xGH_0_1*xGH_1_0*xGH_1_1 - xGH_0_1*xGH_1_0 - xGH_0_1*xGH_1_1 - xGH_1_0*xGH_1_1 + xGH_0_1 + xGH_1_0 + xGH_1_1 - 1, xGH_0_0*xGH_1_0*xGH_1_1 - xGH_0_0*xGH_1_0 - xGH_0_0*xGH_1_1 - xGH_1_0*xGH_1_1 + xGH_0_0 + xGH_1_0 + xGH_1_1 - 1, xGH_0_0*xGH_0_1*xGH_1_1 - xGH_0_0*xGH_0_1 - xGH_0_0*xGH_1_1 - xGH_0_1*xGH_1_1 + xGH_0_0 + xGH_0_1 + xGH_1_1 - 1, xGH_0_0*xGH_0_1*xGH_1_0 - xGH_0_0*xGH_0_1 - xGH_0_0*xGH_1_0 - xGH_0_1*xGH_1_0 + xGH_0_0 + xGH_0_1 + xGH_1_0 - 1]
        """
        I = self.ideal(**kwds)
        if use_symmetries:
            GB = symmStd(I, self.symmetries())
        else:
            GB = I.groebner_basis(algorithm='libsingular:slimgb')
        I.groebner_basis.set_cache(GB)
        return GB

    @cached_method
    def f(self, reduced=True):
        r"""
        Return the polynomial `f` we want to be sos.

        INPUT:

        - ``reduced`` -- if set, then `f` is reduced by the
          groebner basis of :meth:`ideal`.

        EXAMPLES::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex')
            sage: iv.ideal().groebner_basis()
            [xG_0 - 1, xG_1 - 1, xH_0 - 1, xH_1 - 1,
             eG_0_1, eH_0_1,
             xGH_0_0 - 1, xGH_0_1 - 1, xGH_1_0 - 1, xGH_1_1 - 1]
            sage: iv.f(reduced=False)
            xGH_0_0 + xGH_0_1 + xGH_1_0 + xGH_1_1 - 4
            sage: iv.f()
            0

        ::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  invert_xg=True)
            sage: iv.ideal().groebner_basis()
            [xG_0, xG_1, xH_0, xH_1, eG_0_1, eH_0_1,
             xGH_0_0, xGH_0_1, xGH_1_0, xGH_1_1]
            sage: iv.f(reduced=False)
            -xGH_0_0 - xGH_0_1 - xGH_1_0 - xGH_1_1
            sage: iv.f()
            0
        """
        nG = self.G.n
        kG = self.G.k
        nH = self.H.n
        kH = self.H.k
        xGH = self.GH.verticesGH

        if self.invert_xg:
            f = (nG * nH - kG * kH) - sum(xGH)
        else:
            f = sum(xGH) - kG * kH

        if not reduced:
            return f

        I = self.ideal()
        return I.reduce(f)


def all_monomials(P, max_degree):
    r"""
    EXAMPLES::

        sage: P.<x,y> = QQ[]
        sage: all_monomials(P, 3)
        (1, y, x, y^2, x*y, x^2, y^3, x*y^2, x^2*y, x^3)
    """
    ngens = P.ngens()
    exponents = IntegerListsLex(max_sum=max_degree, length=ngens)

    return tuple(sorted(P.monomial(*e) for e in exponents))


def upper_pairs(L):
    r"""
    EXAMPLES::

        sage: list(upper_pairs(srange(4)))
        [(0, 0), (0, 1), (0, 2), (0, 3),
         (1, 1), (1, 2), (1, 3),
         (2, 2), (2, 3),
         (3, 3)]
    """
    return iter((a, b)
                for i, a in enumerate(L)
                for j, b in enumerate(L)
                if j >= i)


def de_gauss(m):
    r"""
    Return `n` such that `n(n+1)/2=m`.
    """
    return ZZ(sqrt(8*m+1)/2-1/2)


class SOS(object):

    def __init__(self, iv, ell, f=None, I=None):
        self.iv = iv
        self.I = iv.ideal() if I is None else I
        self.f = iv.f() if f is None else f
        self.ell = ell

    @cached_method
    def monomials(self, reduce_monomials=True):
        logging = getLogger('vizing')

        ell = self.ell
        I = self.I
        P = I.ring()
        M = all_monomials(P, ell)
        if reduce_monomials:
            RM = tuple(m for m in M if m == I.reduce(m))
            logging.info('using %s monomials (reduced from %s)',
                         len(RM), len(M))
            return RM
        else:
            logging.info('using %s monomials', len(M))
            return M

    def lhs_and_rhs(self, **kwds):
        r"""
        Return the polynomial equation `f = m^T A m`, where `m` are
        determined by :meth:`monomials`.

        EXAMPLES::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex')
            sage: sos = SOS(iv, ell=1)
            sage: sos.lhs_and_rhs()
            (0, A_0_0)

        ::

            sage: iv = VizingIdeal(nG=3, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex')
            sage: sos = SOS(iv, ell=1)
            sage: sos.lhs_and_rhs(reduce_monomials=False)
            (xGH_0_0 + xGH_0_1 + xGH_1_0 + xGH_1_1 + xGH_2_0 + xGH_2_1 - 4,
            (2*A_6_14 - 2*A_6_15)*xG_1*xGH_0_0
             + (2*A_5_14 - 2*A_5_15)*xG_1*xGH_0_1
             + (2*A_4_14 - 2*A_4_15)*xG_1*xGH_1_0
             + (2*A_3_14 - 2*A_3_15)*xG_1*xGH_1_1
             + (2*A_2_14 - 2*A_2_15)*xG_1*xGH_2_0
             + (2*A_1_14 - 2*A_1_15)*xG_1*xGH_2_1
             + (2*A_8_13 - 2*A_8_14 - 2*A_9_13 + 2*A_9_15
                + 2*A_10_14 - 2*A_10_15)*xG_2*eG_1_2
             + (2*A_6_13 - 2*A_6_15)*xG_2*xGH_0_0
             + (2*A_5_13 - 2*A_5_15)*xG_2*xGH_0_1
             + (2*A_4_13 - 2*A_4_15)*xG_2*xGH_1_0
             + (2*A_3_13 - 2*A_3_15)*xG_2*xGH_1_1
             + (2*A_2_13 - 2*A_2_15)*xG_2*xGH_2_0
             + (2*A_1_13 - 2*A_1_15)*xG_2*xGH_2_1
             + (2*A_6_9 - 2*A_6_10)*eG_0_2*xGH_0_0
             + (2*A_5_9 - 2*A_5_10)*eG_0_2*xGH_0_1
             + (2*A_4_8 - 2*A_4_10)*eG_1_2*xGH_1_0
             + (2*A_3_8 - 2*A_3_10)*eG_1_2*xGH_1_1
             + (2*A_2_8 - 2*A_2_9)*eG_1_2*xGH_2_0
             + (2*A_1_8 - 2*A_1_9)*eG_1_2*xGH_2_1
             + 2*A_5_6*xGH_0_0*xGH_0_1
             + 2*A_3_6*xGH_0_0*xGH_1_1
             + 2*A_1_6*xGH_0_0*xGH_2_1
             + 2*A_4_5*xGH_0_1*xGH_1_0
             + 2*A_2_5*xGH_0_1*xGH_2_0
             + 2*A_3_4*xGH_1_0*xGH_1_1
             + 2*A_1_4*xGH_1_0*xGH_2_1
             + 2*A_2_3*xGH_1_1*xGH_2_0
             + 2*A_1_2*xGH_2_0*xGH_2_1
             + (2*A_0_14 - 2*A_0_15 + 2*A_10_14 - 2*A_10_15
                + 2*A_11_14 - 2*A_11_15 + 2*A_12_14 - 2*A_12_15
                + 2*A_13_14 - 2*A_13_15 + A_14_14 - A_15_15)*xG_1
             + (2*A_0_13 - 2*A_0_15 + 2*A_9_13 - 2*A_9_15
                + 2*A_11_13 - 2*A_11_15 + 2*A_12_13 - 2*A_12_15
                + A_13_13 + 2*A_13_14 - 2*A_14_15 - A_15_15)*xG_2
             + (2*A_0_9 - 2*A_0_10 + 2*A_1_9 - 2*A_1_10 + 2*A_2_9
                - 2*A_2_10 + 2*A_3_9 - 2*A_3_10 + 2*A_4_9 - 2*A_4_10
                + A_9_9 + 2*A_9_11 + 2*A_9_12 + 2*A_9_13 + 2*A_9_14
                - A_10_10 - 2*A_10_11 - 2*A_10_12 - 2*A_10_13 - 2*A_10_14)*eG_0_2
             + (2*A_0_8 - 2*A_0_10 + 2*A_1_9 - 2*A_1_10 + 2*A_2_9
                - 2*A_2_10 + 2*A_5_8 - 2*A_5_10 + 2*A_6_8 - 2*A_6_10
                + A_8_8 + 2*A_8_11 + 2*A_8_12 + 2*A_8_14 + 2*A_8_15
                + 2*A_9_13 - 2*A_9_15 - A_10_10 - 2*A_10_11
                - 2*A_10_12 - 2*A_10_13 - 2*A_10_14)*eG_1_2
             + (2*A_0_6 + 2*A_2_6 + 2*A_4_6 + A_6_6 + 2*A_6_10
                + 2*A_6_11 + 2*A_6_12 + 4*A_6_15)*xGH_0_0
             + (2*A_0_5 + 2*A_1_5 + 2*A_3_5 + A_5_5 + 2*A_5_10
                + 2*A_5_11 + 2*A_5_12 + 4*A_5_15)*xGH_0_1
             + (2*A_0_4 + 2*A_2_4 + A_4_4 + 2*A_4_6 + 2*A_4_10
                + 2*A_4_11 + 2*A_4_12 + 4*A_4_15)*xGH_1_0
             + (2*A_0_3 + 2*A_1_3 + A_3_3 + 2*A_3_5 + 2*A_3_10
                + 2*A_3_11 + 2*A_3_12 + 4*A_3_15)*xGH_1_1
             + (2*A_0_2 + A_2_2 + 2*A_2_4 + 2*A_2_6 + 2*A_2_9
                + 2*A_2_11 + 2*A_2_12 + 4*A_2_15)*xGH_2_0
             + (2*A_0_1 + A_1_1 + 2*A_1_3 + 2*A_1_5 + 2*A_1_9
                + 2*A_1_11 + 2*A_1_12 + 4*A_1_15)*xGH_2_1
             + A_0_0 + 2*A_0_10 + 2*A_0_11 + 2*A_0_12 + 4*A_0_15
             - 2*A_1_3 - 2*A_1_5 - 2*A_1_9 + 2*A_1_10 - 2*A_2_4
             - 2*A_2_6 - 2*A_2_9 + 2*A_2_10 - 2*A_3_5 - 2*A_4_6
             - 2*A_9_13 + 2*A_9_15 + A_10_10 + 2*A_10_11 + 2*A_10_12
             + 2*A_10_13 + 2*A_10_15 + A_11_11 + 2*A_11_12 + 4*A_11_15
             + A_12_12 + 4*A_12_15 - 2*A_13_14 + 2*A_13_15 + 2*A_14_15
             + 2*A_15_15)

        ::

            sage: iv = VizingIdeal(nG=3, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  invert_xg=True)
            sage: sos = SOS(iv, ell=1)
            sage: sos.lhs_and_rhs(reduce_monomials=False)
            (-xGH_0_0 - xGH_0_1 - xGH_1_0 - xGH_1_1 - xGH_2_0 - xGH_2_1 + 2,
             (2*A_6_14 - 2*A_6_15)*xG_1*xGH_0_0
             + (2*A_5_14 - 2*A_5_15)*xG_1*xGH_0_1
             + (2*A_4_14 - 2*A_4_15)*xG_1*xGH_1_0
             + (2*A_3_14 - 2*A_3_15)*xG_1*xGH_1_1
             + (2*A_2_14 - 2*A_2_15)*xG_1*xGH_2_0
             + (2*A_1_14 - 2*A_1_15)*xG_1*xGH_2_1
             + (2*A_8_13 - 2*A_8_14 - 2*A_9_13 + 2*A_9_15
                + 2*A_10_14 - 2*A_10_15)*xG_2*eG_1_2
             + (2*A_6_13 - 2*A_6_15)*xG_2*xGH_0_0
             + (2*A_5_13 - 2*A_5_15)*xG_2*xGH_0_1
             + (2*A_4_13 - 2*A_4_15)*xG_2*xGH_1_0
             + (2*A_3_13 - 2*A_3_15)*xG_2*xGH_1_1
             + (2*A_2_13 - 2*A_2_15)*xG_2*xGH_2_0
             + (2*A_1_13 - 2*A_1_15)*xG_2*xGH_2_1
             + (2*A_6_9 - 2*A_6_10)*eG_0_2*xGH_0_0
             + (2*A_5_9 - 2*A_5_10)*eG_0_2*xGH_0_1
             + (2*A_4_8 - 2*A_4_10)*eG_1_2*xGH_1_0
             + (2*A_3_8 - 2*A_3_10)*eG_1_2*xGH_1_1
             + (2*A_2_8 - 2*A_2_9)*eG_1_2*xGH_2_0
             + (2*A_1_8 - 2*A_1_9)*eG_1_2*xGH_2_1
             + 2*A_5_6*xGH_0_0*xGH_0_1 + 2*A_3_6*xGH_0_0*xGH_1_1
             + 2*A_1_6*xGH_0_0*xGH_2_1 + 2*A_4_5*xGH_0_1*xGH_1_0
             + 2*A_2_5*xGH_0_1*xGH_2_0 + 2*A_3_4*xGH_1_0*xGH_1_1
             + 2*A_1_4*xGH_1_0*xGH_2_1 + 2*A_2_3*xGH_1_1*xGH_2_0
             + 2*A_1_2*xGH_2_0*xGH_2_1
             + (2*A_0_14 - 2*A_0_15 + 2*A_10_14 - 2*A_10_15 + A_14_14 - A_15_15)*xG_1
             + (2*A_0_13 - 2*A_0_15 + 2*A_9_13 - 2*A_9_15 + A_13_13 - A_15_15)*xG_2
             + (2*A_0_9 - 2*A_0_10 + A_9_9 + 2*A_9_15 - A_10_10 - 2*A_10_15)*eG_0_2
             + (2*A_0_8 - 2*A_0_10 + A_8_8 + 2*A_8_14 - A_10_10 - 2*A_10_14)*eG_1_2
             + (2*A_0_6 + A_6_6 + 2*A_6_10 + 2*A_6_15)*xGH_0_0
             + (2*A_0_5 + A_5_5 + 2*A_5_10 + 2*A_5_15)*xGH_0_1
             + (2*A_0_4 + A_4_4 + 2*A_4_10 + 2*A_4_15)*xGH_1_0
             + (2*A_0_3 + A_3_3 + 2*A_3_10 + 2*A_3_15)*xGH_1_1
             + (2*A_0_2 + A_2_2 + 2*A_2_9 + 2*A_2_15)*xGH_2_0
             + (2*A_0_1 + A_1_1 + 2*A_1_9 + 2*A_1_15)*xGH_2_1
             + A_0_0 + 2*A_0_10 + 2*A_0_15 + A_10_10 + 2*A_10_15 + A_15_15)
        """
        ell = self.ell
        I = self.I
        P = I.ring()
        base_ring = P.base_ring()

        lhs = I.reduce(self.f)

        monomials = self.monomials(**kwds)
        coefficient_ring = InfinitePolynomialRing(base_ring, names=('A',),
                                                  order='degrevlex')
        Agen = coefficient_ring.gen(0)
        self.A = tuple(Agen[a(i, j)]
                       for i, j in upper_pairs(range(len(monomials))))

        Q = P.change_ring(coefficient_ring)
        iterA = iter(self.A)

        one = Q(1)
        two = Q(2)
        def convert_to_Q(g):
            r"""
            Return Q(g), but faster in our set-up.
            """
            return Q({exponent: coefficient_ring(coefficient)
                      for exponent, coefficient in iteritems(g.dict())})
        rhs = sum(convert_to_Q(I.reduce(m*m_prime))
                  * next(iterA)
                  * (one if m == m_prime else two)
                  for m, m_prime in upper_pairs(monomials))

        return lhs, rhs

    @staticmethod
    def extract_equations(lhs, rhs):
        r"""
        Return the equation system obtained by
        equating the coefficients ("Koeffizientenvergleich").

        EXAMPLES::

            sage: iv = VizingIdeal(nG=2, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex')
            sage: sos = SOS(iv, ell=1)
            sage: lhs, rhs = sos.lhs_and_rhs()
            sage: SOS.extract_equations(lhs, rhs)
            [A_0_0]

        ::

            sage: iv = VizingIdeal(nG=3, kG=2, nH=2, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  invert_xg=True)
            sage: sos = SOS(iv, ell=1)
            sage: lhs, rhs = sos.lhs_and_rhs(reduce_monomials=False)
            sage: SOS.extract_equations(lhs, rhs)
            [2*A_6_14 - 2*A_6_15,
             2*A_5_14 - 2*A_5_15,
             2*A_4_14 - 2*A_4_15,
             2*A_3_14 - 2*A_3_15,
             2*A_2_14 - 2*A_2_15,
             2*A_1_14 - 2*A_1_15,
             2*A_8_13 - 2*A_8_14 - 2*A_9_13 + 2*A_9_15 + 2*A_10_14 - 2*A_10_15,
             2*A_6_13 - 2*A_6_15,
             2*A_5_13 - 2*A_5_15,
             2*A_4_13 - 2*A_4_15,
             2*A_3_13 - 2*A_3_15,
             2*A_2_13 - 2*A_2_15,
             2*A_1_13 - 2*A_1_15,
             2*A_6_9 - 2*A_6_10,
             2*A_5_9 - 2*A_5_10,
             2*A_4_8 - 2*A_4_10,
             2*A_3_8 - 2*A_3_10,
             2*A_2_8 - 2*A_2_9,
             2*A_1_8 - 2*A_1_9,
             2*A_5_6,
             2*A_3_6,
             2*A_1_6,
             2*A_4_5,
             2*A_2_5,
             2*A_3_4,
             2*A_1_4,
             2*A_2_3,
             2*A_1_2,
             2*A_0_14 - 2*A_0_15 + 2*A_10_14 - 2*A_10_15 + A_14_14 - A_15_15,
             2*A_0_13 - 2*A_0_15 + 2*A_9_13 - 2*A_9_15 + A_13_13 - A_15_15,
             2*A_0_9 - 2*A_0_10 + A_9_9 + 2*A_9_15 - A_10_10 - 2*A_10_15,
             2*A_0_8 - 2*A_0_10 + A_8_8 + 2*A_8_14 - A_10_10 - 2*A_10_14,
             2*A_0_6 + A_6_6 + 2*A_6_10 + 2*A_6_15 + 1,
             2*A_0_5 + A_5_5 + 2*A_5_10 + 2*A_5_15 + 1,
             2*A_0_4 + A_4_4 + 2*A_4_10 + 2*A_4_15 + 1,
             2*A_0_3 + A_3_3 + 2*A_3_10 + 2*A_3_15 + 1,
             2*A_0_2 + A_2_2 + 2*A_2_9 + 2*A_2_15 + 1,
             2*A_0_1 + A_1_1 + 2*A_1_9 + 2*A_1_15 + 1,
             A_0_0 + 2*A_0_10 + 2*A_0_15 + A_10_10 + 2*A_10_15 + A_15_15 - 2]
        """
        difference = rhs - lhs
        return difference.coefficients()

    @staticmethod
    def _build_matrix_vector_(M, v, ring, nrows, ncols):
        return matrix(ring, nrows, ncols, M), vector(ring, nrows, v)

    @staticmethod
    def sdp_data(equations, variables, sparse=True):
        r"""
        Return a coefficient matrix and a right hand side vector
        from the given ``equations``

        INPUT:

        - ``equations`` -- a system of polynomials

        - ``sparse`` -- if ``True`` then a dictionary containig the data
          is returned.

        EXAMPLES::

            sage: P.<A_0_0, A_0_1, A_1_1> = QQ[]
            sage: equations = [A_0_0 + A_1_1 - 3, 2*A_0_1 + A_1_1 - 4]
            sage: SOS.sdp_data(equations, P.gens())
            ({(0, 0): 1, (0, 3): 1, (1, 1): 1, (1, 2): 1,
              (1, 3): 1}, {0: 3, 1: 4})
            sage: SOS.sdp_data(equations, P.gens(), sparse=False)
            (
            [1 0 0 1]
            [0 1 1 1], (3, 4)
            )
        """
        from six import iteritems

        P = equations[0].parent()
        m = de_gauss(len(variables))
        variables_to_indices = {v: tuple(ZZ(s) for s in str(v).split('_')[1:])
                                for v in variables}
                               # variables_to_indices = {A_i_j: (i, j)}
        variables_to_enum = {v: ((i*(m+1),) if i == j else (i*m+j, j*m+i))
                             for v, (i, j) in iteritems(variables_to_indices)}
                            # variables_to_enum {A_i_j: (mu, nu)}
                            # where mu, nu are the positions in the
                            # list (A_0_0, A_0_1, ..., A_0_m,
                            #       A_1_0, ..., A_1_m,
                            #       A_m_0, ..., A_n_m)

        def weight(v):
            i, j = variables_to_indices[v]
            if i == j:
                return 1
            else:
                return 1/2

        assert all(eqn.degree() <= 1      # We do a sanity check that
                   for eqn in equations)  # we only have linear terms.

        coefficient_matrix_data = \
            {(e, j): coefficient * weight(monomial)
             for e, eqn in enumerate(equations)
             for coefficient, monomial in eqn
             if monomial != 1
             for j in variables_to_enum[monomial]}
        rhs_vector_data = \
            {e: -coefficient
             for e, eqn in enumerate(equations)
             for coefficient, monomial in eqn
             if monomial == 1}

        if sparse:
            coefficient_matrix_data.setdefault(
                (len(equations)-1, m^2-1), 0)
            rhs_vector_data.setdefault(len(equations)-1, 0)
            return coefficient_matrix_data, rhs_vector_data
        else:
            return SOS._build_matrix_vector_(
                coefficient_matrix_data, rhs_vector_data,
                P.base_ring(), len(equations), m^2)

    @staticmethod
    def write_octave(filename, data):
        with open(filename, 'w') as f:
            if isinstance(data, dict):
                from six import iteritems
                for key, value in iteritems(data):
                    if isinstance(key, list):
                        key = tuple(key)
                    if not isinstance(key, tuple):
                        key = (key, 0)
                    assert len(key) == 2
                    key = (key[0]+1, key[1]+1)  # shift (SageMath-->Octave indices)
                    f.write('{} {}   {}\n'.format(key[0], key[1], value))
                return
            try:
                for row in data.rows():
                    f.write(str(row)[1:-1] + '\n')
                return
            except AttributeError:
                pass
            raise TypeError()

    @staticmethod
    def read_octave(filename, ring=RR):
        with open(filename, 'r') as f:
            data = tuple(tuple(ring(e) for e in line.split())
                         for line in f.readlines())
        return matrix(data)

    @staticmethod
    def alpha(g, i, n, ring=None,
              pi_g=None, pi_i=None, signs_i=None):
        r"""
        EXAMPLES::

            sage: def A(n):
            ....:    return matrix(SR, n, n-1,
            ....:        [SOS.alpha(g, i, n)
            ....:         for g in srange(1, n+1) for i in srange(1, n)])

            sage: A(1)
            []
            sage: A(2)
            [ 1]
            [-1]
            sage: A(3)
            [           0      sqrt(2)]
            [   sqrt(3/2) -1/2*sqrt(2)]
            [  -sqrt(3/2) -1/2*sqrt(2)]
            sage: A(4)
            [           0            0      sqrt(3)]
            [           0  2*sqrt(2/3) -1/3*sqrt(3)]
            [     sqrt(2)   -sqrt(2/3) -1/3*sqrt(3)]
            [    -sqrt(2)   -sqrt(2/3) -1/3*sqrt(3)]
            sage: A(5)
            [              0               0               0               2]
            [              0               0    1/2*sqrt(15)            -1/2]
            [              0      sqrt(10/3)   -1/6*sqrt(15)            -1/2]
            [      sqrt(5/2) -1/2*sqrt(10/3)   -1/6*sqrt(15)            -1/2]
            [     -sqrt(5/2) -1/2*sqrt(10/3)   -1/6*sqrt(15)            -1/2]
            sage: A(6)
            [             0              0              0              0        sqrt(5)]
            [             0              0              0    2*sqrt(6/5)   -1/5*sqrt(5)]
            [             0              0    3*sqrt(1/2) -1/2*sqrt(6/5)   -1/5*sqrt(5)]
            [             0              2     -sqrt(1/2) -1/2*sqrt(6/5)   -1/5*sqrt(5)]
            [       sqrt(3)             -1     -sqrt(1/2) -1/2*sqrt(6/5)   -1/5*sqrt(5)]
            [      -sqrt(3)             -1     -sqrt(1/2) -1/2*sqrt(6/5)   -1/5*sqrt(5)]
        """
        ring_n = n if ring is None else ring(n)

        def _alpha_(g, i):
            if g + i < n:
                return 0
            elif g + i == n:
                result = sqrt(ring_n - 1 - sum(_alpha_(g, j)^2
                                               for j in srange(n-g+1, n)))
                if signs_i is not None:
                    result = signs_i[i] * result
                return result
            else:  # g + i > n
                return -_alpha_(n - i, i) / i

        if pi_g is not None:
            g = pi_g(g)
        if pi_i is not None:
            i = pi_i(i)
        return _alpha_(g, i)

    def is_correct_certificate(self, s):
        iv = self.iv
        sf = iv.polynomial_ring(sum(si^2 for si in s))
        rf = iv.ideal().reduce(sf)
        return iv.f() == rf

    def certificate_kGnG_kHnHm1(self, ring=SR,
                                pi_g=None, pi_i=None, signs_i=None):
        r"""
        SOS-certificate for kG=nG and kH=nH-1

        EXAMPLES::

            sage: iv = VizingIdeal(nG=3, kG=3, nH=4, kH=3,
            ....:                  base_ring=QQ, order='deglex')
            sage: sos = SOS(iv, ell=1)
            sage: tuple(si.expand().simplify().expand() for si in
            ....:       sos.certificate_kGnG_kHnHm1(
            ....:           pi_g=Permutation([2,3,1]),
            ....:           signs_i={1: 1, 2: -1}))
            (1/3*sqrt(3)*xGH_0_0 + 1/3*sqrt(3)*xGH_0_1 + 1/3*sqrt(3)*xGH_0_2
             + 1/3*sqrt(3)*xGH_0_3 + 1/3*sqrt(3)*xGH_1_0 + 1/3*sqrt(3)*xGH_1_1
             + 1/3*sqrt(3)*xGH_1_2 + 1/3*sqrt(3)*xGH_1_3 + 1/3*sqrt(3)*xGH_2_0
             + 1/3*sqrt(3)*xGH_2_1 + 1/3*sqrt(3)*xGH_2_2 + 1/3*sqrt(3)*xGH_2_3
             - 3*sqrt(3),
             1/2*sqrt(2)*xGH_0_0 + 1/2*sqrt(2)*xGH_0_1
             + 1/2*sqrt(2)*xGH_0_2 + 1/2*sqrt(2)*xGH_0_3
             - 1/2*sqrt(2)*xGH_1_0 - 1/2*sqrt(2)*xGH_1_1
             - 1/2*sqrt(2)*xGH_1_2 - 1/2*sqrt(2)*xGH_1_3,
             1/6*sqrt(3)*sqrt(2)*xGH_0_0 + 1/6*sqrt(3)*sqrt(2)*xGH_0_1
             + 1/6*sqrt(3)*sqrt(2)*xGH_0_2 + 1/6*sqrt(3)*sqrt(2)*xGH_0_3
             + 1/6*sqrt(3)*sqrt(2)*xGH_1_0 + 1/6*sqrt(3)*sqrt(2)*xGH_1_1
             + 1/6*sqrt(3)*sqrt(2)*xGH_1_2 + 1/6*sqrt(3)*sqrt(2)*xGH_1_3
             - 1/3*sqrt(3)*sqrt(2)*xGH_2_0 - 1/3*sqrt(3)*sqrt(2)*xGH_2_1
             - 1/3*sqrt(3)*sqrt(2)*xGH_2_2 - 1/3*sqrt(3)*sqrt(2)*xGH_2_3)

        ::

            sage: s = sos.certificate_kGnG_kHnHm1(
            ....:           ring=QQbar,
            ....:           pi_g=Permutation([2,3,1]),
            ....:           signs_i={1: 1, 2: -1})
            sage: sf = iv.polynomial_ring(sum(si^2 for si in s)); sf
            xGH_0_0^2 + 2*xGH_0_0*xGH_0_1 + 2*xGH_0_0*xGH_0_2
            + 2*xGH_0_0*xGH_0_3 + xGH_0_1^2 + 2*xGH_0_1*xGH_0_2
            + 2*xGH_0_1*xGH_0_3 + xGH_0_2^2 + 2*xGH_0_2*xGH_0_3 + xGH_0_3^2
            + xGH_1_0^2 + 2*xGH_1_0*xGH_1_1 + 2*xGH_1_0*xGH_1_2
            + 2*xGH_1_0*xGH_1_3 + xGH_1_1^2 + 2*xGH_1_1*xGH_1_2
            + 2*xGH_1_1*xGH_1_3 + xGH_1_2^2 + 2*xGH_1_2*xGH_1_3
            + xGH_1_3^2 + xGH_2_0^2 + 2*xGH_2_0*xGH_2_1 + 2*xGH_2_0*xGH_2_2
            + 2*xGH_2_0*xGH_2_3 + xGH_2_1^2 + 2*xGH_2_1*xGH_2_2
            + 2*xGH_2_1*xGH_2_3 + xGH_2_2^2 + 2*xGH_2_2*xGH_2_3 + xGH_2_3^2
            - 6*xGH_0_0 - 6*xGH_0_1 - 6*xGH_0_2 - 6*xGH_0_3 - 6*xGH_1_0
            - 6*xGH_1_1 - 6*xGH_1_2 - 6*xGH_1_3 - 6*xGH_2_0 - 6*xGH_2_1
            - 6*xGH_2_2 - 6*xGH_2_3 + 27
            sage: rf = iv.ideal().reduce(sf); rf
            xGH_0_0 + xGH_0_1 + xGH_0_2 + xGH_0_3
            + xGH_1_0 + xGH_1_1 + xGH_1_2 + xGH_1_3
            + xGH_2_0 + xGH_2_1 + xGH_2_2 + xGH_2_3 - 9
            sage: iv.f() == rf
            True

            sage: sf == sum(si^2 for si in sos.certificate_kGnG_kHnHm1_simple())
            True
        """
        iv = self.iv
        assert iv.G.n == iv.G.k and iv.H.n - 1 == iv.H.k

        P = iv.polynomial_ring
        x = iv.GH.vertexGH
        n = iv.G.n

        def xalpha(g, i):
            if i == 0:
                return 1
            return SOS.alpha(g+1, i, n, ring=ring,
                             pi_g=pi_g, pi_i=pi_i, signs_i=signs_i)

        return tuple((sum(xalpha(g, i)
                          * sum(x(g, h) for h in iv.H.vertices_names)
                          for g in iv.G.vertices_names)
                      - (iv.G.k * iv.H.k if i == 0 else 0)) / sqrt(ring(n))
                     for i in srange(n))

    def certificate_kGnG_kHnHm1_simple(self):
        r"""
        SOS-certificate for kG=nG and kH=nH-1

        EXAMPLES::

            sage: iv = VizingIdeal(nG=3, kG=3, nH=4, kH=3,
            ....:                  base_ring=QQ, order='deglex')
            sage: sos = SOS(iv, ell=1)
            sage: s = sos.certificate_kGnG_kHnHm1_simple()
            sage: sf = sum(si^2 for si in s)
            sage: rf = iv.ideal().reduce(sf); rf
            xGH_0_0 + xGH_0_1 + xGH_0_2 + xGH_0_3
            + xGH_1_0 + xGH_1_1 + xGH_1_2 + xGH_1_3
            + xGH_2_0 + xGH_2_1 + xGH_2_2 + xGH_2_3 - 9
            sage: iv.f() == rf
            True
        """
        iv = self.iv
        assert iv.G.n == iv.G.k and iv.H.n - 1 == iv.H.k

        x = iv.GH.vertexGH
        return tuple(sum(x(g,h) for h in iv.H.vertices_names) - iv.H.k
                     for g in iv.G.vertices_names)

    def certificate_nG3_kG2_nH3_kH2(self):
        r"""
        SOS-certificate for nG=3, kG=2, nG=3, kG=2

        EXAMPLES::

            sage: iv = VizingIdeal(nG=3, kG=2, nH=3, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  fix_dominating_set=True)
            sage: sos = SOS(iv, ell=1)
            sage: s = sos.certificate_nG3_kG2_nH3_kH2()
            sage: sf = iv.polynomial_ring(sum(si^2 for si in s))
            sage: rf = iv.ideal().reduce(sf); rf
            xGH_0_0 + xGH_0_1 + xGH_0_2 + xGH_1_0 + xGH_1_1 + xGH_1_2 + xGH_2_0 + xGH_2_1 + xGH_2_2 - 4
            sage: iv.f() == rf
            True
            sage: sos.is_correct_certificate(s)
            True
        """
        iv = self.iv
        assert iv.G.n == 3 and iv.G.k == 2 and iv.H.n == 3 and iv.H.k == 2

        x = SR.var('x')
        R = QQ.extension([x^2-2, x^2-3], ['sqrt2', 'sqrt3'])
        sqrt2, sqrt3 = R.gens()
        alpha = 2/3*sqrt2
        beta = 1/18*sqrt2*sqrt3
        gamma = 1/3*sqrt2
        delta = 1/6*sqrt2
        epsilon = 0
        phi = 1/9*sqrt3
        sigma = 2*sqrt2
        mu = 2/9*sqrt3
        
        x = iv.GH.vertexGH
        g1, g2, g3 = iv.G.vertices_names

        s1 = (epsilon
              + phi * sum(x(g, h)
                          for g in iv.G.vertices_names
                          for h in iv.H.vertices_names)
              - mu * sum(x(g, ha) * x(g, he)
                         for g in iv.G.vertices_names
                         for ha, he in iv.H.edges_names))
        s2 = (0
              + delta * sum(x(g2, h) for h in iv.H.vertices_names)
              - delta * sum(x(g3, h) for h in iv.H.vertices_names)
              - 2*delta * sum(x(g2, ha) * x(g2, he)
                              for ha, he in iv.H.edges_names)
              + 2*delta * sum(x(g3, ha) * x(g3, he)
                              for ha, he in iv.H.edges_names))
        s3 = (0
              - 2*beta * sum(x(g1, h) for h in iv.H.vertices_names)
              + beta * sum(x(g2, h) for h in iv.H.vertices_names)
              + beta * sum(x(g3, h) for h in iv.H.vertices_names)
              + 4*beta * sum(x(g1, ha) * x(g1, he)
                             for ha, he in iv.H.edges_names)
              - 2*beta * sum(x(g2, ha) * x(g2, he)
                             for ha, he in iv.H.edges_names)
              - 2*beta * sum(x(g3, ha) * x(g3, he)
                             for ha, he in iv.H.edges_names))
        s4 = (sigma
              - alpha * sum(x(g, h)
                            for g in iv.G.vertices_names
                            for h in iv.H.vertices_names)
              + gamma * sum(x(g, ha) * x(g, he)
                            for g in iv.G.vertices_names
                            for ha, he in iv.H.edges_names))
        return (s1, s2, s3, s4)

    def certificate_nG_kGnGm1_3_2_simple(self):
        r"""
        SOS-certificate for kG=nG-1, nG=3, kG=2

        EXAMPLES::

            sage: iv = VizingIdeal(nG=3, kG=2, nH=3, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  fix_dominating_set=True)
            sage: sos = SOS(iv, ell=1)
            sage: s = sos.certificate_nG_kGnGm1_3_2_simple()
            sage: sf = iv.polynomial_ring(sum(si^2 for si in s))
            sage: rf = iv.ideal().reduce(sf); rf
            xGH_0_0 + xGH_0_1 + xGH_0_2 + xGH_1_0 + xGH_1_1 + xGH_1_2 + xGH_2_0 + xGH_2_1 + xGH_2_2 - 4
            sage: iv.f() == rf
            True
            sage: sos.is_correct_certificate(s)
            True

            sage: iv = VizingIdeal(nG=4, kG=3, nH=3, kH=2,
            ....:                  base_ring=QQ, order='deglex',
            ....:                  fix_dominating_set=True)
            sage: sos = SOS(iv, ell=1)
            sage: s = sos.certificate_nG_kGnGm1_3_2_simple()
            sage: sos.is_correct_certificate(s)
            True

        """
        iv = self.iv
        assert iv.G.k == iv.G.n - 1 and iv.H.n == 3 and iv.H.k == 2

        x = SR.var('x')
        R = QQ.extension(x^2-2, 'sqrt2')
        sqrt2, = R.gens()
        alpha = -2/3*sqrt2
        gamma = 1/3*sqrt2
        sigma = sqrt2 * (iv.G.n - 1)

        x = iv.GH.vertexGH

        s0 = (sigma
              + alpha * sum(x(g, h)
                            for g in iv.G.vertices_names
                            for h in iv.H.vertices_names)
              +gamma * sum(x(g, ha) * x(g, he)
                           for g in iv.G.vertices_names
                           for ha, he in iv.H.edges_names))
        return (s0,) + \
            tuple(
                (sum(x(g, h) for h in iv.H.vertices_names)
                 - 2 * sum(x(g, ha) * x(g, he) for ha, he in iv.H.edges_names)) / 3
                for g in iv.G.vertices_names)

    def certificate_playground(self):
        iv = self.iv

        sigma, alpha, gamma, rho = iv.GH.additional_variables

        x = iv.GH.vertexGH
        def iter_hahe():
            for a, ha in enumerate(iv.H.vertices_names):
                for e, he in enumerate(iv.H.vertices_names):
                    if a >= e:
                        continue
                    yield (ha, he)

        s0 = (sigma
              + alpha * sum(x(g, h)
                            for g in iv.G.vertices_names
                            for h in iv.H.vertices_names)
              + gamma * sum(x(g, ha) * x(g, he)
                            for g in iv.G.vertices_names
                            for ha, he in iv.H.edges_names))
        return (s0,) + \
            tuple(
                (sum(x(g, h) for h in iv.H.vertices_names)
                 - 2 * sum(x(g, ha) * x(g, he) for ha, he in iv.H.edges_names)) * rho
                for g in iv.G.vertices_names)

    def certificate_playground4343(self):
        iv = self.iv

        vars = iv.GH.additional_variables
        sigma, alpha, gamma, pi = vars[:4]
        beta_g = vars[4:8]
        delta_g = vars[8:12]
        eps_g = vars[12:16]

        x = iv.GH.vertexGH

        s0 = (sigma
              + alpha * sum(x(g, h)
                            for g in iv.G.vertices_names
                            for h in iv.H.vertices_names)
              + gamma * sum(x(g, ha) * x(g, he)
                            for g in iv.G.vertices_names
                            for ha in iv.H.vertices_names
                            for he in iv.H.vertices_names
                            if set([ha, he]) == 2)
              + pi * sum(x(g, ha) * x(g, he) * x(g, hi)
                         for g in iv.G.vertices_names
                         for ha in iv.H.vertices_names
                         for he in iv.H.vertices_names
                         for hi in iv.H.vertices_names
                         if set([ha, he, hi]) == 3))
        return (s0,) + \
            tuple(
                (b * sum(x(g, h) for h in iv.H.vertices_names)
                 + d * sum(x(g, ha) * x(g, he)
                           for ha in iv.H.vertices_names
                           for he in iv.H.vertices_names
                           if set([ha, he]) == 2)
                 + e * sum(x(g, ha) * x(g, he) * x(g, hi)
                            for ha in iv.H.vertices_names
                            for he in iv.H.vertices_names
                            for hi in iv.H.vertices_names
                            if set([ha, he, hi]) == 3))
                for g, b, d, e in zip(iv.G.vertices_names, beta_g, delta_g, eps_g))


def compute_sdp(nG, kG, nH, kH,
                base_ring, order,
                ell=None, ell_min=None, ell_max=None,
                filename_coefficient_matrix=None, filename_rhs_vector=None,
                filename_vizing_ideal=None, filename_monomials=None,
                filename_log=None,
                reduce_monomials=True,
                invert_xg=False,
                fix_dominating_set=False):
    r"""
    Compute the SDP and return result as well as
    write it in octave-readable file.

    EXAMPLES::

        sage: A, b = compute_sdp(nG=2, kG=2, nH=2, kH=2,
        ....:                    ell=1,
        ....:                    base_ring=QQ, order='deglex',
        ....:                    invert_xg=True)
        Traceback (most recent call last):
        ...
        RuntimeError: f == 0 (nothing to do)
        sage: A, b = compute_sdp(nG=3, kG=2, nH=2, kH=2,
        ....:                    ell=1,
        ....:                    base_ring=QQ, order='deglex',
        ....:                    invert_xg=True)
        sage: A.dict(), b.dict()  # showing it sparse
        ({(0, 76): 1, (0, 116): 1, (1, 65): 1, (1, 115): 1,
          (2, 54): 1, (2, 114): 1, (3, 43): 1, (3, 113): 1,
          (4, 32): 1, (4, 112): 1, (5, 21): 1, (5, 111): 1,
          (6, 86): 1, (6, 87): -1, (6, 97): -1, (6, 106): 1, (6, 107): -1, (6, 117): -1,
          (7, 75): 1, (7, 105): 1, (8, 64): 1, (8, 104): 1,
          (9, 53): 1, (9, 103): 1, (10, 42): 1, (10, 102): 1,
          (11, 31): 1, (11, 101): 1, (12, 20): 1, (12, 100): 1,
          (13, 74): 1, (13, 94): 1, (14, 63): 1, (14, 93): 1,
          (15, 51): 1, (15, 81): 1, (16, 40): 1, (16, 80): 1,
          (17, 29): 1, (17, 30): -1, (17, 79): 1, (17, 90): -1,
          (18, 18): 1, (18, 19): -1, (18, 78): 1, (18, 89): -1,
          (19, 61): 1, (19, 71): 1, (20, 39): 1, (20, 69): 1,
          (21, 17): 1, (21, 67): 1, (22, 49): 1, (22, 59): 1,
          (23, 27): 1, (23, 57): 1, (24, 37): 1, (24, 47): 1,
          (25, 15): 1, (25, 45): 1, (26, 25): 1, (26, 35): 1,
          (27, 13): 1, (27, 23): 1,
          (28, 10): 1, (28, 110): 1, (28, 120): 1,
          (29, 9): 1, (29, 97): 1, (29, 99): 1, (29, 107): 1, (29, 108): 1,
          (30, 8): 1, (30, 88): 1, (30, 96): 1,
          (31, 7): 1, (31, 77): 1, (31, 84): 1, (31, 87): 1, (31, 117): 1,
          (32, 6): 1, (32, 66): 1, (32, 72): 1,
          (33, 5): 1, (33, 55): 1, (33, 60): 1,
          (34, 4): 1, (34, 44): 1, (34, 48): 1,
          (35, 3): 1, (35, 33): 1, (35, 36): 1,
          (36, 2): 1, (36, 22): 1, (36, 24): 1, (36, 30): 1, (36, 90): 1,
          (37, 1): 1, (37, 11): 1, (37, 12): 1, (37, 19): 1, (37, 89): 1,
          (38, 0): 1},
         {32: -1, 33: -1, 34: -1, 35: -1, 36: -1, 37: -1, 38: 2})
        sage: result = compute_sdp(nG=3, kG=2, nH=2, kH=2,
        ....:                      ell_min=1,
        ....:                      base_ring=QQ, order='deglex',
        ....:                      invert_xg=True)
        sage: result.keys()
        [1, 2, 3]

        sage: A, b = compute_sdp(nG=3, kG=2, nH=2, kH=2,
        ....:                    ell=1,
        ....:                    base_ring=QQ, order='deglex',
        ....:                    fix_dominating_set=True)
        sage: A.dict(), b.dict()  # showing it sparse
        ({(0, 23): 1, (0, 58): 1,
          (1, 15): 1, (1, 57): 1, (2, 46): 1, (2, 53): 1,
          (3, 14): 1, (3, 49): 1, (4, 21): 1, (4, 42): 1,
          (5, 28): 1, (5, 35): 1, (6, 12): 1, (6, 33): 1,
          (7, 19): 1, (7, 26): 1, (8, 10): 1, (8, 17): 1,
          (9, 7): 1, (9, 31): 1, (9, 39): 1, (9, 47): 1, (9, 55): 1,
          (9, 56): 1, (9, 59): 1, (9, 60): 1, (9, 61): 1, (9, 62): 1,
          (9, 63): 1,
          (10, 6): 1, (10, 22): 1, (10, 30): 1, (10, 38): 1, (10, 48): 1,
          (10, 50): 1, (10, 51): 1, (10, 52): 1, (10, 54): 1,
          (11, 5): 1, (11, 13): 1, (11, 29): 1, (11, 37): 1, (11, 40): 1,
          (11, 41): 1, (11, 43): 1, (11, 44): 1, (11, 45): 1,
          (12, 4): 1, (12, 20): 1, (12, 32): 1, (12, 34): 1, (12, 36): 1,
          (12, 37): 1, (12, 38): 1, (12, 39): 1, (12, 44): 1, (12, 52): 1,
          (12, 60): 1,
          (13, 3): 1, (13, 11): 1, (13, 24): 1, (13, 25): 1, (13, 27): 1,
          (13, 29): 1, (13, 30): 1, (13, 31): 1, (13, 43): 1, (13, 51): 1,
          (13, 59): 1,
          (14, 2): 1, (14, 16): 1, (14, 18): 1, (14, 20): 1, (14, 22): 1,
          (14, 34): 1, (14, 50): 1,
          (15, 1): 1, (15, 8): 1, (15, 9): 1, (15, 11): 1, (15, 13): 1,
          (15, 25): 1, (15, 41): 1,
          (16, 0): 1, (16, 11): -1, (16, 13): -1, (16, 20): -1, (16, 22): -1,
          (16, 25): -1, (16, 29): -1, (16, 30): -1, (16, 31): -1, (16, 34): -1,
          (16, 37): -1, (16, 38): -1, (16, 39): -1, (16, 41): -1, (16, 43): -1,
          (16, 44): -1, (16, 50): -1, (16, 51): -1, (16, 52): -1, (16, 59): -1,
          (16, 60): -1},
         {10: 1, 11: 1, 12: 1, 13: 1, 14: 1, 15: 1, 16: -4})
    """
    logger = getLogger('vizing')
    if ell is None == ell_min is None:
        raise ValueError('both or none, ell and ell_min, are specified')
    ell_string = '='+str(ell) if ell_min is None else '>='+str(ell_min)
    def insert_parameters(s, ell_string):
        if s is None:
            return s
        return s.format(
            nG=nG, kG=kG, nH=nH, kH=kH,
            ell=ell_string.replace('>=', 'ge').replace('=', ''),
            base_ring=base_ring, order=order,
            reduce_monomials=reduce_monomials,
            invert_xg=invert_xg,
            fix_dominating_set=fix_dominating_set)
    logger.filename(insert_parameters(filename_log, ell_string))

    logger.info('start (nG=%s, kG=%s, nH=%s, kH=%s, ell%s)',
                nG, kG, nH, kH, ell_string)

    logger.info('constructing the ideal I...')
    iv = VizingIdeal(nG, kG, nH, kH,       # an object containing data
                     base_ring=base_ring,  # related to the graphs
                     order=order,
                     invert_xg=invert_xg,
                     fix_dominating_set=fix_dominating_set)
    logger.info('working in polynomial ring with %s variables',
                iv.polynomial_ring.ngens())
    I = iv.ideal()  # the ideal
    logger.info('ideal I generated by %s polynomials', I.ngens())
    logger.debug('I = %s', I)

    logger.info('computing groebner basis (polynomials over %s, order=%s)...',
                base_ring, order)
    GB = iv.groebner_basis()
    logger.info('groebner basis GB with size %s found', len(GB))
    logger.debug('GB = %s', GB)

    logger.info('generating f mod I...')
    f = iv.f()  # the polynomial f (mod I)
                # (for later testing f >= 0 (mod I))
    logger.debug('f = %s', f)

    if filename_vizing_ideal is not None:
        filename = insert_parameters(filename_vizing_ideal, ell_string)
        logger.info('vizing ideal: writing "%s"...', filename)
        save(iv, filename)

    if f == 0:
        logger.info('f == 0 (nothing to do)')
        raise RuntimeError('f == 0 (nothing to do)')

    result = {}
    if ell_min is None:
        ells = (ell,)
    elif ell_max is not None:
        ells = range(ell_min, ell_max)
    else:
        import itertools
        ells = itertools.count(ell_min)
    number_of_monomials = -1

    for ell in ells:
        ell = ZZ(ell)

        logger.info('creating sum-of-squares object (ell=%s)...', ell)
        sos = SOS(iv, ell)  # an object containing the data for checking
                                # if f is a sum of squares
        monomials = sos.monomials(reduce_monomials=reduce_monomials)
        if len(monomials) == number_of_monomials:
            logger.info('stopping (ell<%s): number of monomials (%s) '
                        'did not change since last iteration',
                        ell, number_of_monomials)
            break
        number_of_monomials = len(monomials)

        if filename_monomials is not None:
            filename = insert_parameters(filename_monomials, str(ell))
            logger.info('monomials: writing "%s"...', filename)
            save(monomials, filename)

        lhs, rhs = sos.lhs_and_rhs(             # equation f = m^T A m (mod I)
            reduce_monomials=reduce_monomials)  # for a monomial basis m
        equations = sos.extract_equations(lhs, rhs)  # the equation system by
                                                     # equating the coefficients
                                                     # ("Koeffizientenvergleich")
        ngensA = len(sos.A)
        logger.info('%s equations in %s variables (from f = m^T A m)',
                    len(equations), ngensA)
        logger.debug('equations (from f = m^T A m): %s', equations)

        coefficient_matrix, rhs_vector = sos.sdp_data(equations, sos.A)  # convert/extract

        if filename_coefficient_matrix is not None:
            filename = insert_parameters(filename_coefficient_matrix, str(ell))
            logger.info('coefficient matrix: writing "%s"...', filename)
            sos.write_octave(filename, coefficient_matrix)
        if filename_rhs_vector is not None:
            filename = insert_parameters(filename_rhs_vector, str(ell))
            logger.info('right hand side vector: writing "%s"...', filename)
            sos.write_octave(filename, rhs_vector)

        result[ell] = sos._build_matrix_vector_(
            coefficient_matrix, rhs_vector,
            base_ring, len(equations), de_gauss(ngensA)^2)
        logger.info('done sum-of-squares object (ell=%s)', ell)
    else:
        logger.info('stopping: all desired ell in %s processed ', ells)

    if ell_min is None:
        ell_string_extended = 'ell' + ell_string
    else:
        ell_string_extended = str(ell) + '>ell' + ell_string
    logger.info('done (nG=%s, kG=%s, nH=%s, kH=%s, %s)',
                nG, kG, nH, kH, ell_string_extended)

    if ell_min is None:
        return result[ell]
    else:
        return result
