# load command line arguments
import sys
if len(sys.argv) == 1:
    raise ValueError('command line arguments: '
                     'nG kG nH kH [ell | ell_min True] **kwds')

kwds = {'order': 'deglex',
        'invert_xg': False,
        'fix_dominating_set': True,
        'reduce_monomials': True,
        'ell': None,
        'ell_min': None,
        'ell_max': None,
        'base_ring': QQ}

def relaxed_sage_eval(s):
    try:
        return sage_eval(s)
    except NameError:
        return s

args = tuple(sage_eval(a) for a in sys.argv[1:] if '=' not in a)
kwds['nG'] = args[0]
kwds['kG'] = args[1]
kwds['nH'] = args[2]
kwds['kH'] = args[3]
if len(args) == 5:
    kwds['ell'] = args[4]
elif len(args) == 6:
    if args[5] != True:
        raise ValueError('5th positional argument is not "True"')
    kwds['ell_min'] = args[4]
elif len(args) > 6:
    raise ValueError('too many positional arguments given')

kwds.update({key: relaxed_sage_eval(b)
             for key, b in (a.split('=', 1)
                            for a in sys.argv[1:] if '=' in a)})

# load main code
load('vizing.sage')

# set-up logging
import logging
logger = getLogger('vizing')
logger.level(logging.INFO)  # less talkative
#logger.level(logging.DEBUG)  # more talkative

# set-up path/filenames
path = '../data-fix/'
import os
if not os.path.exists(path):
    os.makedirs(path)
filename_prefix = (path
                   + 'sdp_nG{nG}_kG{kG}_nH{nH}_kH{kH}'
                   + '_ell{ell}'
                   + '_{order}'
                   + '_red{reduce_monomials}'
                   + '_inv{invert_xg}'
                   + '_fix{fix_dominating_set}')  # expressions in {...} will be replaced

kwds['filename_coefficient_matrix'] = filename_prefix + '_A'
kwds['filename_rhs_vector'] = filename_prefix + '_b'
kwds['filename_vizing_ideal'] = filename_prefix + '_viz.sobj'
kwds['filename_monomials'] = filename_prefix + '_mon.sobj'
kwds['filename_log'] = filename_prefix + '.log'

# compute
Asbs = compute_sdp(**kwds)

# profiling
#load('./vizing.sage'); runsnake("compute_sdp(nG=3, kG=2, nH=2, kH=2, ell=3, base_ring=QQ, order='deglex', reduce_monomials=True)")
