# Vizing’s Conjecture via Semidefinite Programming and Sums-of-Squares

This repository contains code accompanying the articles
* [**An Optimization-Based Sum-of-Squares Approach to Vizing's Conjecture**](https://arxiv.org/abs/1901.10288)
* **Towards a Computational Proof of Vizing’s Conjecture using Semidefinite Programming and Sums-of-Squares**

by [Elisabeth Gaar](https://www.aau.at/team/gaar-elisabeth/),
[Daniel Krenn](http://www.danielkrenn.at/),
[Susan Margulies](https://www.usna.edu/Users/math/margulies/)
and [Angelika Wiegele](http://wwwu.aau.at/anwiegel/).